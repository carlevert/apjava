/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the player and game info.
 * 
 * Game.java
 * 
 * Written by: Carl-Evert Kangas
 * 
 * Created: 26/11-14
 */

package antitd;

import java.util.ArrayList;

import model.Level;
import model.LevelsXmlParser;
import model.World;

/**
 * This class is for the game. The user info, what level the user is
 * on and such.
 * 
 * @author Carl-Evert Kangas dv14cks
 * @since 26/11-14
 *
 */
public class Game {

    /** The canvas to paint on. */
    private GameCanvas canvas;

    /** The credits the user has. */
    private int credit;

    /** The score needed to complete a level. */
    private int goalScore;

    /** The users score. */
    private int score;

    /** The name of the levels file. */
    private String levelsFilename;

    /** The world in which the game is run. */
    private World world;

    /** The Level that is played. */
    private Level level;

    /** The level number. */
    private int currentLevel = 0;

    /** The list of all levels. */
    private ArrayList<Level> levels;

    /** The boolean to keep track on if the games is paused. */
    private boolean pause = false;

    /**
     * This constructor generates a level from the LevelsXmlParser
     * class and sets this to be the level in the world.
     * 
     * @param levelsFilename
     *            The name of the xml file with levels.
     */
    public Game(String levelsFilename) {
	this.levelsFilename = levelsFilename;
	this.levels = new LevelsXmlParser(levelsFilename).getLevels();
	this.level = levels.get(currentLevel);
	world = new World(this.level, this);
	setCredit(level.getStartCredit());
	setGoalScore(level.getGoalScore());
    }

    /**
     * This is the default constructor which takes the levels.xml from
     * the class path and calls the other constructor with it.
     */
    public Game() {
	this("levels.xml");
    }

    /**
     * This method restarts the game from the beginning.
     */
    public void restartGame() {
	levels = new LevelsXmlParser(levelsFilename).getLevels();
	currentLevel = 0;
	this.score = 0;
	restartLevel();
    }

    /**
     * This method resets the field and restarts the level.
     */
    public void restartLevel() {
	levels = new LevelsXmlParser(levelsFilename).getLevels();
	this.level = levels.get(currentLevel);
	this.world = new World(level, this);
	setCredit(level.getStartCredit());
	setGoalScore(level.getGoalScore());
    }

    /**
     * This returns true/false depending on if there is a next level.
     * 
     * @return true/false
     */
    public boolean hasNextLevel() {
	return (currentLevel + 1) < levels.size();
    }

    /**
     * This method changes the current level to the next one.
     */
    public void nextLevel() {
	currentLevel++;
	if (currentLevel < levels.size()) {
	    this.level = levels.get(currentLevel);
	    this.world = new World(this.level, this);
	    setCredit(level.getStartCredit());
	    setGoalScore(level.getGoalScore());
	}
    }

    /**
     * This method return the number of credits the user has.
     * 
     * @return credit
     */
    public int getCredit() {
	return credit;
    }

    /**
     * This method return the score needed to complete a level.
     * 
     * @return goalScore
     */
    public int getGoalScore() {
	return goalScore;
    }

    /**
     * This method returns the world that the game is running in.
     * 
     * @return world
     */
    public World getWorld() {
	return world;
    }

    /**
     * This method returns true/false depending on if the game is
     * paused or not.
     * 
     * @return true/false.
     */
    public boolean isPaused() {
	return this.pause;
    }

    /**
     * This method sets the nr of credits the user has.
     * 
     * @param credit
     *            The value the credits will be set to.
     */
    public void setCredit(int credit) {
	this.credit = credit;
    }

    /**
     * This method sets the canvas on which the game will be painted.
     * 
     * @param canvas
     *            The canvas to paint on.
     */
    public void setGameCanvas(GameCanvas canvas) {
	this.canvas = canvas;
    }

    /**
     * This method sets the score that the user needs to complete a
     * level.
     * 
     * @param goalScore
     *            The value of the score.
     */
    public void setGoalScore(int goalScore) {
	this.goalScore = goalScore;
    }

    /**
     * This method sets whether or not the game is running.
     * 
     * @param running
     *            Boolean to tell if the game is running or not.
     */
    public void setPaused(boolean running) {
	this.pause = running;
    }

    /**
     * This method returns the current score.
     * 
     * @return score
     */
    public int getScore() {
	return this.score;
    }

    /**
     * This method adds the value from the parameter to the score.
     * 
     * @param score
     *            This is the value to be added
     */
    public void addScore(int score) {
	this.score += score;
    }

    /**
     * This method returns the canvas of the game field.
     * 
     * @return canvas.
     */
    public GameCanvas getCanvas() {
	return canvas;
    }
}
