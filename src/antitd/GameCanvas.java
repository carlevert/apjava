/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for displaying the game field.
 * 
 * GameCanvas.java
 * 
 * Written by: Ludvig Bostr�m and Carl-Evert Kangas
 * 
 * Created: 27/11-14
 */

package antitd;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JComponent;

import model.Level;
import model.SwitchTile;
import model.Tile;

/**
 * This class is for the canvas to be painted on.
 * 
 * @author Ludvig Bostrom c13lbm
 * @author Carl-Evert Kangas dv14cks
 * @since 27/11-14
 */
@SuppressWarnings("serial")
public class GameCanvas extends JComponent implements MouseListener {

    /** Rows on the canvas. */
    private final int ROWS = 16;

    /** Columns on the canvas. */
    private final int COLS = 20;

    /** The size of a tile. */
    public static final int TILESIZE = 32;

    /** The font used for text. */
    private Font font;

    /** The game that will be painted. */
    private Game game;

    /**
     * This constructor sets up the font and canvas.
     * 
     * @param game
     *            The game to paint.
     */
    public GameCanvas(Game game) {
	super();
	this.game = game;
	game.setGameCanvas(this);
	setPreferredSize(new Dimension(COLS * TILESIZE, ROWS * TILESIZE));
	setDoubleBuffered(true);
	addMouseListener(this);
	try {
	    InputStream inFont =
		    getClass().getClassLoader().getResourceAsStream(
		        "res/pixel.ttf");
	    Font font = Font.createFont(Font.TRUETYPE_FONT, inFont);
	    this.font = font.deriveFont(16f);
	} catch (FontFormatException | IOException e) {
	    System.err.print("Error occured: " + e + "\n");
	}
    }

    /*
     * Code copied from:
     * http://stackoverflow.com/questions/4413132/problems
     * -with-newline -in- graphics2d-drawstring
     */
    /**
     * This method draws the text to the canvas.
     * 
     * @param g
     *            The graphics to paint to.
     * @param text
     *            The text to paint.
     * @param x
     *            The pixel coordinate to paint at.
     * @param y
     *            The pixel coordinate to paint at.
     */
    private void drawString(Graphics2D g, String text, int x, int y) {
	for (String line : text.split("\n"))
	    g.drawString(line, -10 + TILESIZE * COLS
		               - g.getFontMetrics().stringWidth(line), y +=
		    (g.getFontMetrics().getHeight() - 2));
    }

    /**
     * This method paints the canvas.
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public void paint(Graphics g) {
	Graphics2D g2d = (Graphics2D) g;
	if (game.isPaused())
	    return;
	g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	    RenderingHints.VALUE_ANTIALIAS_ON);
	g2d.setStroke(new BasicStroke(0.2f));

	g2d.setColor(new Color(80, 90, 100, 255));

	for (int c = 0; c < COLS; c++)
	    for (int r = 0; r < ROWS; r++)
		g2d.fillRect(c * TILESIZE, r * TILESIZE, TILESIZE, TILESIZE);

	game.getWorld().paint(g2d);

	String scorestr =
	        "LEVEL SCORE: " + game.getWorld().getLevelScore() + " - NEED "
	                + game.getGoalScore();
	scorestr =
	        scorestr + ")\nTOTAL SCORE: " + game.getScore() + "\nCREDITS: "
	                + game.getCredit();

	g2d.setFont(font);
	g2d.setColor(java.awt.Color.BLACK);
	drawString(g2d, scorestr, 10, 4);
	g2d.setColor(java.awt.Color.WHITE);
	drawString(g2d, scorestr, 9, 3);
    }

    /**
     * This method checks which tile is clicked and if it is a switch
     * tile the next tile is changed.
     * 
     * @author Ludvig Bostrom c13lbm
     * @author Carl-Evert Kangas dv14cks
     */
    @Override
    public void mouseClicked(MouseEvent e) {
	int x = e.getX() / TILESIZE;
	int y = e.getY() / TILESIZE;
	Level level = game.getWorld().getLevel();
	Tile[][] tiles = level.getTiles();
	if (tiles.length > 0 && tiles[0].length > 0 && x < tiles[0].length
	    && y < tiles.length && tiles[y][x] instanceof SwitchTile) {
	    ((SwitchTile) tiles[y][x]).toggle();
	}
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
