/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for reading andd writing to the server.
 * 
 * HighscoreHandler.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 4/12-14
 */

package antitd;

import java.sql.*;

/**
 * This class is for handling the highscore on the database server.
 * The database used is "postgresql".
 * 
 * @author Leonard Jakobsson, c13ljn
 * @since 4/12-14
 */
public class HighscoreHandler {

    /** Used to send queries. */
    private Statement statement;

    /** Formats to correct and safe querys. */
    private PreparedStatement insertStatement;

    /**
     * This constructor connects to the database server and makes the
     * tables if not existent.
     */
    public HighscoreHandler() {

	String insertQuery =
	        "INSERT INTO Highscores (Name, Points) " + "values (?, ?)";
	String createTableQuery =
	        "CREATE TABLE IF NOT EXISTS "
	                + "Highscores (Name VARCHAR(20), Points INT)";
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://postgres.cs.umu.se/c5dv135_ht14_g8";
	String user = "c5dv135_ht14_g8";
	String pass = "weitutai";

	try {
	    Class.forName(driver);
	} catch (ClassNotFoundException e) {
	    System.err.print("Could not find database server: " + driver + "\n");
	}

	try {
	    Connection con = DriverManager.getConnection(url, user, pass);

	    statement = con.createStatement();
	    statement.execute(createTableQuery);
	    insertStatement = con.prepareStatement(insertQuery);

	} catch (SQLException e) {
	    System.err.print("Error occured: " + e + "\n");
	}

    }

    /**
     * This method adds the highscore given to the database.
     * 
     * @param h
     *            The highscore to add.
     */
    public void addHighscore(Highscore h) {
	try {
	    insertStatement.setString(1, h.getName());
	    insertStatement.setInt(2, h.getPoints());
	    insertStatement.executeUpdate();
	} catch (SQLException e) {
	    System.err.print("Error occured: " + e + "\n");
	}
    }

    /**
     * This method removes a certain highscore.
     * 
     * @param h
     *            The highscore to remove.
     */
    public void removeHighscore(Highscore h) {
	String deleteQuery =
	        "DELETE FROM Highscores WHERE " + "Name='" + h.getName()
	                + "' AND Points=" + h.getPoints();
	try {
	    statement.execute(deleteQuery);
	} catch (SQLException e) {
	    System.err.print("Error occured: " + e + "\n");
	}
    }

    /**
     * This method returns the top n scores in an array of Highscores.
     * 
     * @param n
     *            The amount of top scores wanted.
     * @return scores
     */
    public Highscore[] getTopNScores(int n) {

	Highscore[] scores = new Highscore[n];
	String selectQuery = "SELECT * FROM Highscores ORDER BY Points DESC";
	try {
	    ResultSet res = statement.executeQuery(selectQuery);
	    for (int i = 0; i < n; i++) {

		if (res.next()) {
		    Highscore hs =
			    new Highscore(res.getString("Name"),
			        res.getInt("Points"));
		    scores[i] = hs;
		}
	    }
	} catch (SQLException e) {
	    System.err.print("Error occured: " + e + "\n");
	}

	return scores;
    }

    /**
     * Warning: this drops the highscore table! Should be used with
     * caution! Used for debugging and reseting the highscores.
     */
    public void dropHighscoresTable() {
	try {
	    statement.execute("DROP TABLE Highscores");
	} catch (SQLException e) {
	    System.err.print("Error occured: " + e + "\n");
	}
    }

}
