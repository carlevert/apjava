/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the highscore
 * 
 * Highscore.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 4/12-14
 */

package antitd;

/**
 * This class represents is a higscore.
 * 
 * @author Leonard Jakobsson, c13ljn
 * @since 4/12-14
 */
public class Highscore {

    /** The name of the player. */
    private String name;

    /** The points the player got. */
    private int points;

    /**
     * This constructor sets up the highscore of the player.
     * 
     * @param name
     *            The players name
     * @param points
     *            The points the player got
     */
    public Highscore(String name, int points) {
	this.name = name;
	this.points = points;
    }

    /**
     * This method returns the name of the player.
     * 
     * @return name
     */
    public String getName() {
	return name;
    }

    /**
     * This method returns the points.
     * 
     * @return points
     */
    public int getPoints() {
	return points;
    }

}
