/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for displaying the highscore.
 * 
 * HighscoreFrame.java
 * 
 * Written by: Leonard Jakobsson and Carl-Evert Kangas
 * 
 * Created: 4/12-14
 */

package antitd;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * This class is for the input and output of the highscore from the
 * user interface.
 * 
 * @author Leonard Jakobsson c13ljn
 * @author Carl-Evert Kangas dv14cks
 * @since 4/12-2014
 * 
 */
public class HighscoreFrame {

    /** The JFrame of the highscore window. */
    private JFrame frame;

    /** The handler to add and read from server. */
    private HighscoreHandler hsHandler;

    /** The points of the user. */
    private int points;

    /** Boolean to check from where the highscore frame is called. */
    private boolean calledFromMenu = false;

    /**
     * Constructs a HighscoreFrame with a HighscoreHandler to manage
     * highscores. Call createEnterFrame(int points) to enter a
     * highscore and then see the top 10 resulsts, or
     * createShowFrame() to see the top 10 results.
     * 
     * @author Leonard Jakobsson c13ljn
     */
    public HighscoreFrame() {
	hsHandler = new HighscoreHandler();
    }

    /**
     * Constructor made to check form where the highscore framed is
     * opened.
     * 
     * @param calledFromMenu
     *            boolean to see if its called from the menu or not
     * @author Carl-Evert Kangas dv14cks
     */
    public HighscoreFrame(boolean calledFromMenu) {
	this();
	this.calledFromMenu = calledFromMenu;
    }

    /**
     * Call to create a frame to enter a highscore and then displays
     * the top 10 highscores
     * 
     * @param points
     *            The amount of points of the player.
     * 
     * @author Leonard Jakobsson c13ljn
     */
    public void createEnterFrame(int points) {
	this.points = points;
	constructEnterFrame();
    }

    /**
     * The input dialog to write your name.
     * 
     * @author Leonard Jakobsson c13ljn
     */
    private void constructEnterFrame() {
	String ans;
	boolean ok;
	do {
	    ok = true;
	    ans =
		    JOptionPane.showInputDialog(null,
		        "Enter your name (maximum of 20 characters)",
		        "Enter your name", JOptionPane.PLAIN_MESSAGE);
	    if (ans == null) {
		JOptionPane.showMessageDialog(null, "Invalid name: "
		                                    + "Must enter a name",
		    "Invalid name", JOptionPane.ERROR_MESSAGE);
		ok = false;
		continue;
	    }
	    if (ans.length() > 20) {
		JOptionPane.showMessageDialog(null,
		    "Invalid name: "
		            + "Name must not be more than 20 characters!",
		    "Invalid name", JOptionPane.ERROR_MESSAGE);
		ok = false;
	    }
	} while (!ok);
	enterHighscore(ans);
	createShowFrame();
    }

    /**
     * This method adds a highscore with the highscore handler.
     * 
     * @param name
     *            The name of the player
     * 
     * @author Leonard Jakobsson c13ljn
     */
    private void enterHighscore(String name) {
	Highscore nh = new Highscore(name, points);
	hsHandler.addHighscore(nh);
    }

    /**
     * Call to display the top 10 highscores.
     * 
     * @author Leonard Jakobsson c13ljn
     */
    @SuppressWarnings("serial")
    public void createShowFrame() {
	JButton button;
	JTable table;
	Highscore[] top10Highscores;

	top10Highscores = new Highscore[10];
	top10Highscores = hsHandler.getTopNScores(10);

	// display the top 10 highscores in a JTable, if not 10
	// highscores is available "-----" is filled in the remaining
	// table cells
	table = new JTable();
	table.setModel(new DefaultTableModel() {
	    @Override
	    public boolean isCellEditable(int row, int column) {
		// all cells should be non-editable
		return false;
	    }
	});
	DefaultTableModel tm = (DefaultTableModel) table.getModel();
	tm.setRowCount(10);
	String[] columnNames = { "Name", "Score" };
	tm.setColumnIdentifiers(columnNames);
	int i = 0;
	for (Highscore h : top10Highscores) {
	    if (h == null)
		break;

	    tm.setValueAt(h.getName(), i, 0);
	    tm.setValueAt(h.getPoints(), i, 1);
	    i++;
	}
	for (; i < 10; i++) {
	    tm.setValueAt("-----", i, 0);
	    tm.setValueAt("-----", i, 1);
	}

	button = new JButton("Close");
	button.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent arg0) {
		frame.dispose();
		if (!calledFromMenu) {
		    int ans =
			    JOptionPane.showConfirmDialog(null, "Play again?",
			        "Restart?", JOptionPane.YES_NO_OPTION);
		    if (ans != JOptionPane.YES_OPTION)
			System.exit(0);
		}

	    }
	});

	frame = new JFrame("Highscore");
	frame.add(table, BorderLayout.CENTER);
	frame.add(button, BorderLayout.SOUTH);
	frame.pack();
	// sets a little wider width on the frame so the title fits
	Dimension dim = new Dimension();
	dim.setSize(frame.getSize().getWidth() + 50, frame.getSize()
	                                                  .getHeight());
	frame.setPreferredSize(dim);
	frame.pack();
	frame.setVisible(true);
    }

}