/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the panel with the players controls.
 * 
 * ControlPanel.java
 * 
 * Written by: Ludvig Bostr�m and Carl-Evert Kangas
 * 
 * Created: 1/12-14
 */

package antitd;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * 
 * This class is for the control panel from which the user will
 * control the game.
 * 
 * @author Ludvig Bostrom c13lbm
 * @author Carl-Evert Kangas dv14cks
 * @since 1/12-14
 *
 */
@SuppressWarnings("serial")
public class ControlPanel extends JPanel {

    /** The button that sends the troops. */
    private JButton dropTroopButton;

    /** The button to place teleport tiles. */
    private JButton setTeleTiles;

    /** The combo box with the three kinds of troops. */
    private JComboBox<String> troopSelectCombobox;

    /** The combobox with the living teleport troops. */
    private JComboBox<String> teleportTroopCombobox;

    /** The game with the user info. */
    private Game game;

    /**
     * The key is the name of the troop and the value is the nr of
     * tiles that has been placed.
     */
    private HashMap<String, Integer> placedTiles;

    /**
     * This constructor sets up a panel with its components and
     * listeners. These components are for sending troops and placing
     * teleport tiles.
     * 
     * @param game
     *            The game that contains the information about what is
     *            going on.
     * @author Ludvig Bostrom c13lbm
     * @author Carl-Evert Kangas dv14cks
     */
    public ControlPanel(Game game) {
	super();
	this.game = game;
	placedTiles = new HashMap<String, Integer>();

	dropTroopButton = new JButton("Drop troop");
	dropTroopButton.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		int len =
		        troopSelectCombobox.getSelectedItem().toString()
		                           .lastIndexOf('(');

		ControlPanel.this.game.getWorld().addTroop(
		    "model."
		            + troopSelectCombobox.getSelectedItem().toString()
		                                 .substring(0, len - 1).trim());
	    }
	});
	add(dropTroopButton);

	/*
	 * These strings should be set in an other way. Did not have
	 * time to change.
	 */
	String[] troopTypes =
	        new String[] { "StrongTroop (100)", "FastTroop (100)",
	                      "TeleportTroop (400)" };
	troopSelectCombobox = new JComboBox<>(troopTypes);
	add(troopSelectCombobox);

	setTeleTiles = new JButton("Set Teleporter-tiles");
	setTeleTiles.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		String troop =
		        ControlPanel.this.teleportTroopCombobox.getSelectedItem()
		                                               .toString();

		int end = placedTiles.get(troop);
		if (end == 0 || end == 1) {

		    ControlPanel.this.game.getWorld().placeTeleport(troop, end);
		    placedTiles.put(troop, end + 1);
		    if (end == 1) {
			setTeleTiles.setEnabled(false);
			setTeleTiles.setContentAreaFilled(false);
		    }
		}
	    }

	});
	add(setTeleTiles);
	setTeleTiles.setEnabled(false);
	setTeleTiles.setContentAreaFilled(false);

	teleportTroopCombobox = new JComboBox<String>();

	teleportTroopCombobox.setPreferredSize(new Dimension(120, 26));
	add(teleportTroopCombobox);
	teleportTroopCombobox.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {

		if (((JComboBox<?>) e.getSource()).getItemCount() > 1) {

		    String troop =
			    ((JComboBox<?>) e.getSource()).getSelectedItem()
			                                  .toString();

		    int nrPlaced = placedTiles.get(troop);
		    if (nrPlaced == 2) {
			setTeleTiles.setEnabled(false);
			setTeleTiles.setContentAreaFilled(false);
		    } else {
			setTeleTiles.setEnabled(true);
			setTeleTiles.setContentAreaFilled(true);
		    }

		} else if (((JComboBox<?>) e.getSource()).getItemCount() == 0) {
		    setTeleTiles.setEnabled(false);
		    setTeleTiles.setContentAreaFilled(false);
		}
	    }
	});

    }

    /**
     * This method enables and disables buttons and combo boxes when
     * paused.
     * 
     * @param paused
     *            Boolean to set if paused or not.
     */
    public void setPaused(boolean paused) {
	boolean enabled = !paused;
	dropTroopButton.setEnabled(enabled);
	troopSelectCombobox.setEnabled(enabled);
	teleportTroopCombobox.setEnabled(enabled);
    }

    /**
     * This method updates the troopCombobox to be the same as the
     * troops available on the field.
     * 
     * @author Ludvig Bostrom c13lbm
     */
    public void updateTroopCombobox() {
	ArrayList<String> list =
	        ControlPanel.this.game.getWorld().getTeleTroops();

	for (int i = 0; i < teleportTroopCombobox.getItemCount(); i++) {

	    if (!list.contains(teleportTroopCombobox.getItemAt(i).toString())) {
		placedTiles.remove(teleportTroopCombobox.getItemAt(i)
		                                        .toString());
		teleportTroopCombobox.removeItemAt(i);
		i--;
	    }
	}

	for (String string : list) {
	    boolean add = true;
	    for (int i = 0; i < teleportTroopCombobox.getItemCount(); i++) {
		if (teleportTroopCombobox.getItemAt(i).equals(string)) {
		    add = false;

		}
	    }
	    if (add) {
		placedTiles.put(string, 0);
		teleportTroopCombobox.addItem(string);
		teleportTroopCombobox.setSelectedItem(string);
		setTeleTiles.setEnabled(true);
		setTeleTiles.setContentAreaFilled(true);

	    }
	}
    }
}
