/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Tests the different tiles.
 * 
 * TileTest.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 24/11-14
 */

package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import model.EndTeleportTile;
import model.PathTile;
import model.StartTeleportTile;
import model.SwitchTile;
import model.Troop;
import model.FastTroop;

import org.junit.Test;

/**
 * Tests all tiles (PathTile, SwitchTile and the TeleportTiles)
 * 
 * @author Leonard Jakobsson c13ljn
 * @since 24/11-14
 */
public class TileTest {
    // declared here to see if it changes in a anonymous class
    // (used in testPathTile())
    private Troop troop;

    /**
     * Tests landOn
     */
    @Test
    public void testPathTile() {
	PathTile pathTile = new PathTile();
	pathTile.setPosition(0, 0);
	pathTile.setNextTile(pathTile);
	troop = new FastTroop(pathTile);
	Observer o = new Observer() {
	    @Override
	    public void update(Observable arg0, Object arg1) {
		assertEquals(arg1, troop);
		troop = null;
	    }
	};
	pathTile.addObserver(o);
	assertNotNull(troop);
	pathTile.landOn(troop);
	assertNull(troop);
    }

    /**
     * Tests toggle
     */
    @Test
    public void testSwitchTile() {
	// 2 exits
	SwitchTile tile1 = new SwitchTile();
	tile1.setPosition(5, 5);
	ArrayList<PathTile> exits1 = new ArrayList<PathTile>();
	PathTile p01 = new PathTile();
	p01.setPosition(5, 4);
	exits1.add(p01);
	PathTile p02 = new PathTile();
	p02.setPosition(5, 6);
	exits1.add(p02);
	tile1.setNextTile(p01);
	tile1.setExits(exits1);
	assertEquals(tile1.getNextTile(), p01);

	PathTile temp1 = tile1.getNextTile();
	for (int i = 0; i < 5; i++) {
	    tile1.toggle();
	    assertNotEquals(tile1.getNextTile(), temp1);
	    temp1 = tile1.getNextTile();
	    assertTrue(exits1.contains(temp1));
	}

	// 3 exits
	SwitchTile tile = new SwitchTile();
	tile.setPosition(1, 1);
	ArrayList<PathTile> exits = new ArrayList<PathTile>();
	PathTile p1 = new PathTile();
	p1.setPosition(2, 1);
	exits.add(p1);
	PathTile p2 = new PathTile();
	p2.setPosition(1, 2);
	exits.add(p2);
	PathTile p3 = new PathTile();
	p3.setPosition(0, 1);
	exits.add(p3);
	tile.setNextTile(p1);
	tile.setExits(exits);
	assertEquals(tile.getNextTile(), p1);

	PathTile temp = tile.getNextTile();
	for (int i = 0; i < 10; i++) {
	    tile.toggle();
	    assertNotEquals(tile.getNextTile(), temp);
	    temp = tile.getNextTile();
	    assertTrue(exits.contains(temp));
	}

    }

    /**
     * Tests StartTeleportTile and EndTeleportTile
     */
    @Test
    public void testTeleportTiles() {
	StartTeleportTile startTile = new StartTeleportTile();
	EndTeleportTile endTile = new EndTeleportTile();
	PathTile temp = new PathTile();

	startTile.setNextTile(temp);
	assertEquals(startTile.getNextTile(), temp);
	assertNotEquals(startTile.getNextTile(), endTile);
	startTile.activate(endTile);
	assertNotEquals(startTile.getNextTile(), temp);
	assertEquals(startTile.getNextTile(), endTile);
    }

}
