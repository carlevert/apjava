/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Tests the different highscore classes.
 * 
 * HighscoreHandlerTest.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 24/11-14
 */

package tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import antitd.Highscore;
import antitd.HighscoreHandler;

/**
 * Tests HighscoreHandler and Highscore.
 * 
 * @author Leonard Jakobsson c13ljn
 * @since 24/11-14
 */
public class HighscoreHandlerTest {

    private HighscoreHandler hsh;

    /**
     * Loads a new HighscoreHandler (with connection)
     * 
     * @throws Exception
     *             If setup "fails", exception is thrown
     */
    @Before
    public void setUp() throws Exception {
	hsh = new HighscoreHandler();
    }

    /**
     * Sets the HighscoreHandler object to null.
     * 
     * @throws Exception
     *             If teardown "fails", exception is thrown
     */
    @After
    public void tearDown() throws Exception {
	hsh = null;
    }

    /**
     * Tests inserting and removing a highscore with the
     * HighscoreHandler
     */
    @Test
    public void testInsertAndRemoveHighscore() {
	Highscore h = new Highscore("TESTNAME", 1);
	hsh.addHighscore(h);
	hsh.removeHighscore(h);
    }

    /**
     * The table "Highscores" should be dropped before or in the
     * beggining of this test if some highscore has the maximum score
     * (2147483647)
     */
    @Test
    public void testGetTopNScores() {
	Highscore h1 =
	        new Highscore("TESTNAME1", (int) Double.POSITIVE_INFINITY);
	Highscore h2 =
	        new Highscore("TESTNAME2", (int) Double.POSITIVE_INFINITY);
	Highscore h3 =
	        new Highscore("TESTNAME3", (int) Double.POSITIVE_INFINITY);

	hsh.addHighscore(h1);
	hsh.addHighscore(h2);
	hsh.addHighscore(h3);

	Highscore[] arr = hsh.getTopNScores(3);

	Boolean h1c = false;
	Boolean h2c = false;
	Boolean h3c = false;
	Boolean duplicates = false;
	for (int i = 0; i < 3; i++) {
	    switch (arr[i].getName()) {
	    case "TESTNAME1":
		if (!h1c) {
		    h1c = true;
		} else {
		    duplicates = true;
		}
		break;
	    case "TESTNAME2":
		if (!h2c) {
		    h2c = true;
		} else {
		    duplicates = true;
		}
		break;
	    case "TESTNAME3":
		if (!h3c) {
		    h3c = true;
		} else {
		    duplicates = true;
		}
		break;
	    }
	}

	assertTrue(h1c);
	assertTrue(h2c);
	assertTrue(h3c);
	assertFalse(duplicates);

	hsh.removeHighscore(h1);
	hsh.removeHighscore(h2);
	hsh.removeHighscore(h3);

    }

}
