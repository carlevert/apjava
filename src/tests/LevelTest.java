/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Tests the methods in level.
 * 
 * LevelTest.java
 * 
 * Written by: Linus Bleckert
 * 
 * Created: 25/11-14
 */

package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Level;
import model.PathTile;
import model.PossibleTowerTile;
import model.Tile;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests Level.
 * 
 * @author Linus Bleckert c13lbt
 * @since 25/11-14
 */
public class LevelTest {

    /**
     * A two-dimensionall array with tiles to represent the map.
     */
    private Tile[][] tileArray;
    /**
     * A level-object to be tested.
     */
    private Level lvl;
    /**
     * A list of possibleTowerTiles
     */
    private ArrayList<PossibleTowerTile> possibleTowerTiles;

    /**
     * The setup for this test
     * 
     * @throws Exception
     *             , if setup fails
     */
    @Before
    public void setUp() throws Exception {
	lvl = new Level();
    }

    /**
     * The teardown for this test
     * 
     * @throws Exception
     *             , if teardown fails
     */
    @After
    public void tearDown() throws Exception {
	lvl = null;
    }

    /**
     * Tests if the tilearray that gets created has the correct
     * information.
     */
    @Test
    public void testTiles() {

	tileArray = new Tile[10][10];
	for (int j = 0; j < 10; j++) {
	    for (int i = 0; i < 10; i++) {

		if ((j == 4) && (i == 4)) {
		    PossibleTowerTile temp = new PossibleTowerTile();
		    temp.setPosition(i, j);
		    tileArray[i][j] = temp;
		} else if ((j == 0) && (i == 0)) {
		    PossibleTowerTile temp = new PossibleTowerTile();
		    temp.setPosition(i, j);
		    tileArray[i][j] = temp;
		} else if ((j == 9) && (i == 9)) {
		    PossibleTowerTile temp = new PossibleTowerTile();
		    temp.setPosition(i, j);
		    tileArray[i][j] = temp;
		} else {
		    PathTile temp = new PathTile();
		    temp.setNextTileX(1);
		    temp.setNextTileY(1);
		    temp.setPosition(i, j);
		    tileArray[i][j] = temp;
		}
	    }
	}
	lvl.setHeight(10);
	lvl.setWidth(10);
	lvl.setTiles(tileArray);
	possibleTowerTiles = lvl.getPossibleTowerTiles();
	assertEquals(possibleTowerTiles.size(), 3);

	assertTrue(possibleTowerTiles.get(0).getPos().getX() == 0);
	assertFalse(possibleTowerTiles.get(1).getPos().getX() == 5);
	assertEquals(possibleTowerTiles.get(2).getPos().getY(), 9);

	assertEquals(((PathTile) tileArray[5][5]).getNextTileX(), 1);
    }
}
