/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Tests the methods in Position.
 * 
 * PositionTest.java
 * 
 * Written by: Ludvig Bostr�m
 * 
 * Created: 24/11-14
 */

package tests;

import static org.junit.Assert.*;
import model.Position;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests the class position
 * 
 * @author Ludvig Bostrom c13lbm
 * @since 24/11-14
 *
 */
public class PositionTest {

    /**
     * An object from the position class
     */
    private Position p;

    /**
     * Setup for the test.
     * 
     * @throws Exception
     *             Thrown if fail
     */
    @Before
    public void setUp() throws Exception {
	p = new Position(1, 1);
    }

    /**
     * Tests getDistance method in the Position-class
     */
    @Test
    public void testGetDistance() {
	Position p2 = new Position(4, 5);
	assertEquals(p.getDistance(p2), 5, 0.0001);
	assertNotEquals(p.getDistance(p2), 4, 0.0001);
    }
}