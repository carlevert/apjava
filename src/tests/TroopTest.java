/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Tests the different methods in the troops.
 * 
 * TroopTest.java
 * 
 * Written by: Linus Bleckert
 * 
 * Created: 24/11-14
 */

package tests;

import static org.junit.Assert.*;
import model.FastTroop;
import model.PathTile;
import model.StrongTroop;
import model.Troop;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests the class Troop and its subclasses.
 * 
 * @author Linus Bleckert c13lbt
 * @since 24/11-14
 */
public class TroopTest {

    /**
     * A tile that the troop can land on.
     */
    private PathTile tile;
    /**
     * Another tile that gets set as next from the previous tile.
     */
    private PathTile nextTile;

    /**
     * The tests setup, creates the tiles and sets one tiles next to
     * the other tile.
     * 
     * @throws Exception
     *             , if setup fails
     */
    @Before
    public void setUp() throws Exception {
	tile = new PathTile();
	nextTile = new PathTile();

	nextTile.setNextTile(null);
	nextTile.setPosition(2, 0);

	tile.setNextTile(nextTile);
	tile.setPosition(0, 0);
	tile.setNextTileX(2);
	tile.setNextTileY(0);

    }

    /**
     * The teardown for the test
     * 
     * @throws Exception
     *             , if teardown fails
     */
    @After
    public void tearDown() throws Exception {
	tile = null;
	nextTile = null;
    }

    /**
     * Creates a troop and tests if the troop got correct attributes,
     * calls for the troops update-method and tests if the troops
     * pixelPositions changed accordingly aswell if the troop changes
     * currenttile correctly when a certain number of updates were
     * called.
     */
    @Test
    public void testStrongTroop() {
	Troop exTroop = new StrongTroop(tile);
	assertSame(exTroop.getNextTile(), nextTile);
	assertSame(exTroop.getCurrentTile(), tile);

	assertEquals(exTroop.getHp(), 65);
	assertTrue(exTroop.getDamage(35));
	assertFalse(exTroop.getDamage(35));
	assertEquals(exTroop.getHp(), 0);

	exTroop.update();

	assertEquals(exTroop.getxPixPos(), 6);
	assertEquals(exTroop.getyPixPos(), 0);

	for (int i = 0; i < 10; i++) {
	    exTroop.update();
	}

	assertSame(exTroop.getCurrentTile(), nextTile);
    }

    /**
     * Same test as previous but with fastTroop.
     */
    @Test
    public void testFastTroop() {
	Troop exTroop = new FastTroop(tile);
	assertSame(exTroop.getNextTile(), nextTile);
	assertSame(exTroop.getCurrentTile(), tile);

	assertEquals(exTroop.getHp(), 35);
	assertTrue(exTroop.getDamage(35));
	assertEquals(exTroop.getHp(), 0);

	exTroop.update();

	assertEquals(exTroop.getxPixPos(), 21);
	assertEquals(exTroop.getyPixPos(), 0);

	for (int i = 0; i < 30; i++) {
	    exTroop.update();
	}

	assertSame(exTroop.getCurrentTile(), nextTile);

    }
}