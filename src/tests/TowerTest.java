/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Tests the different methods in the towers.
 * 
 * TowerTest.java
 * 
 * Written by: Linus Bleckert
 * 
 * Created: 24/11-14
 */

package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.PathTile;
import model.LongRangeTower;
import model.PossibleTowerTile;
import model.StrongTroop;
import model.Troop;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Testclass for the class tower
 * 
 * @author Linus Bleckert c13lbt
 * @since 24/11-14
 */
public class TowerTest {

    /**
     * The tower that gets tested.
     */
    private LongRangeTower t;
    /**
     * A list of targets for the tower.
     */
    private ArrayList<PathTile> targets;
    /**
     * A tile that troops can land on and get shot on.
     */
    private PathTile tile;
    /**
     * Another tile that gets set as next from the previous tile.
     */
    private PathTile nextTile;
    /**
     * A troop to be targeted.
     */
    private Troop exTroop;
    /**
     * A tile that the tower is placed on.
     */
    private PossibleTowerTile towerTile;

    /**
     * The setup for the tests, creates the tower, tiles and the troop
     * that needs for tower to be tested.
     * 
     * @throws Exception
     *             , if setup fails
     */
    @Before
    public void setUp() throws Exception {
	t = new LongRangeTower();
	towerTile = new PossibleTowerTile();
	towerTile.setPosition(4, 4);
	t.setTile(towerTile);

	targets = new ArrayList<PathTile>();
	tile = new PathTile();
	tile.setPosition(5, 5);
	tile.addObserver(t);

	nextTile = new PathTile();
	nextTile.addObserver(t);
	nextTile.setPosition(2, 2);
	tile.setNextTile(nextTile);
	tile.setNextTileX(2);
	tile.setNextTileY(2);

	exTroop = new StrongTroop((PathTile) tile);
	tile.landOn(exTroop);

	targets.add(tile);
	targets.add(nextTile);
	t.setRangeList(targets);

    }

    /**
     * The teardown for the tests.
     * 
     * @throws Exception
     *             , if teardown fails.
     */
    @After
    public void tearDown() throws Exception {
	t = null;
	targets = null;
	tile = null;
	nextTile = null;
	exTroop = null;
	towerTile = null;
    }

    /**
     * Tests the method updateGame, checks if the tower has the
     * correct target and also if the target took damage.
     */
    @Test
    public void testUpdateGame() {
	t.updateGame();

	assertSame(t.getCurrentTarget(), exTroop);
	t.getCurrentTarget().getDamage(10);
	assertEquals(exTroop.getHp(), 55);

	for (int i = 0; i < 15; i++)
	    exTroop.update();

	assertSame(t.getCurrentTarget().getCurrentTile(), nextTile);

    }

    /**
     * Tests the method unAimIfCurrentTarget.
     */
    @Test
    public void testUnAim() {

	t.unaimIfCurrentTarget(exTroop);
	assertEquals(t.getCurrentTarget(), null);
    }
}
