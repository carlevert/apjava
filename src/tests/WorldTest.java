/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Tests the different methods in World.
 * 
 * WorldTest.java
 * 
 * Written by: Linus Bleckert
 * 
 * Created: 25/11-14
 */

package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.PathTile;
import model.PossibleTowerTile;
import model.FastTroop;
import model.Tile;
import model.Level;
import model.Tower;
import model.LongRangeTower;
import model.World;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import antitd.Game;

/**
 * Tests for the class World
 * 
 * @author Linus Bleckert c13lbt
 * @since 25/11-14
 */
public class WorldTest {

    /**
     * A two-dimensionall array with tiles to represent the map.
     */
    private Tile[][] tileArray;
    /**
     * A level-object
     */
    private Level lvl;
    /**
     * A world-object
     */
    private World world;
    /**
     * A tower-object
     */
    private Tower tower;

    /**
     * The setup for this testclass, creates the objects, mainly the
     * two-dimensionall array with tiles that represents a map.
     * 
     * @throws Exception
     *             , if setup fails
     */
    @Before
    public void setUp() throws Exception {
	lvl = new Level();
	tileArray = new Tile[10][10];
	for (int j = 0; j < 10; j++) {
	    for (int i = 0; i < 10; i++) {

		if ((j == 4) && (i == 4)) {
		    PossibleTowerTile temp = new PossibleTowerTile();
		    temp.setPosition(i, j);
		    tileArray[i][j] = temp;
		} else if ((j == 0) && (i == 0)) {
		    PossibleTowerTile temp = new PossibleTowerTile();
		    temp.setPosition(i, j);
		    tileArray[i][j] = temp;
		} else if ((j == 9) && (i == 9)) {
		    PossibleTowerTile temp = new PossibleTowerTile();
		    temp.setPosition(i, j);
		    tileArray[i][j] = temp;
		} else {
		    PathTile temp = new PathTile();
		    temp.setNextTileX(1);
		    temp.setNextTileY(1);
		    temp.setPosition(i, j);
		    tileArray[i][j] = temp;
		}
	    }
	}
	lvl.setHeight(10);
	lvl.setWidth(10);
	lvl.setTiles(tileArray);

	lvl.setStartTile(5, 5);
	world = new World(lvl, new Game());
    }

    /**
     * The teardown for this testclass
     * 
     * @throws Exception
     *             , if teardown fails
     */
    @After
    public void tearDown() throws Exception {
	tileArray = null;
	lvl = null;
	world = null;
	tower = null;

    }

    /**
     * Test for the method generateRangeArray creates an array with
     * tiles that a tower supposedly can target and tests if the array
     * is correct size and contain correct tiles.
     */
    @Test
    public void testGenerateRangeArray() {
	ArrayList<PathTile> tiles;
	tower = new LongRangeTower();
	tiles = world.generateRangeArray(tileArray[4][4], 2, tower);
	assertEquals(12, tiles.size(), 0.01);
	assertSame(tiles.get(3), (PathTile) tileArray[3][5]);

	tiles = world.generateRangeArray(tileArray[0][0], 3, tower);
	assertEquals(10, tiles.size(), 0.01);
	assertSame(tiles.get(5), (PathTile) tileArray[1][2]);

	tiles = world.generateRangeArray(tileArray[9][9], 4, tower);
	assertEquals(16, tiles.size(), 0.01);
	assertSame(tiles.get(7), (PathTile) tileArray[7][9]);
	tiles = null;

    }

    /**
     * Adds two troops to world and tests if the troops got added
     * correctly.
     */
    @Test
    public void testTroopAdd() {

	world.addTroop("model.StrongTroop");
	world.addTroop("model.FastTroop");

	assertEquals(world.getTroops().size(), 2);
	assertTrue(world.getTroops().get(1) instanceof FastTroop);
    }

    /**
     * Places two towers in world and tests if they are there.
     */
    @Test
    public void testPlaceTowers() {

	world.getTowers().clear();
	world.placeTowers(2);

	assertEquals(world.getTowers().size(), 2);
	assertNotNull(world.getTowers().get(0));
	assertNotNull(world.getTowers().get(1));

    }

}
