/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Main class for GUI.
 * 
 * AntiTowerDefence.java
 * 
 * Written by: Carl-Evert Kangas
 * 
 * Created: 24/11-14
 */

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintStream;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import antitd.ControlPanel;
import antitd.Game;
import antitd.GameCanvas;
import antitd.HighscoreFrame;

/**
 * This is the Main class that creates the window and its menu items.
 * It also creates a Game object that is updated and repainted for
 * every time tick in the timer.
 * 
 * @author Carl-Evert Kangas
 * @since 24/11-14
 */
@SuppressWarnings("serial")
public class AntiTowerDefence extends JFrame {

    /** The game object that is controlled from here. */
    private Game game;

    /** The menu item for the new game option. */
    private JMenuItem newGameMenuItem;

    /** The menu item for the restart option. */
    private JMenuItem restartLevelMenuItem;

    /** The menu item for the resume option. */
    private JMenuItem pauseResumeMenuItem;

    /** The timer that ticks for the game. */
    private Timer timer;

    /** The panel from which the user controls the game. */
    private ControlPanel contrPane;

    private boolean running = true;
    
    /**
     * The constructor sets up the window and menu items and starts
     * the game.
     * 
     * @param args
     *            The run arguments, if given use instead of
     *            levels.xml
     */
    public AntiTowerDefence(String args[]) {

	ErrStream err = new ErrStream(this);
	// System.setErr(new PrintStream(err));

	if (args.length == 1 && new File(args[0]).exists())
	    game = new Game(args[0]);
	else
	    game = new Game();

	setTitle("Anti-Tower Defence");
	setDefaultCloseOperation(EXIT_ON_CLOSE);

	JMenuBar menuBar = new JMenuBar();
	JMenu fileMenu = new JMenu("File");

	restartLevelMenuItem = new JMenuItem("Restart level");
	restartLevelMenuItem.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		AntiTowerDefence.this.game.restartLevel();
	    }
	});

	newGameMenuItem = new JMenuItem("New Game");
	newGameMenuItem.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		if (!AntiTowerDefence.this.timer.isRunning()) {
		    AntiTowerDefence.this.contrPane.setPaused(false);
		    AntiTowerDefence.this.timer.start();
		    AntiTowerDefence.this.newGameMenuItem.setText("Restart");
		} else {
		    AntiTowerDefence.this.game.restartGame();
		}

	    }
	});

	pauseResumeMenuItem = new JMenuItem("Pause");
	pauseResumeMenuItem.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		Game g = AntiTowerDefence.this.game;
		if (g.isPaused()) {
		    g.setPaused(false);
		    AntiTowerDefence.this.timer.restart();
		    AntiTowerDefence.this.pauseResumeMenuItem.setText("Pause");
		    AntiTowerDefence.this.contrPane.setPaused(false);
		} else {
		    g.setPaused(true);
		    AntiTowerDefence.this.timer.stop();
		    AntiTowerDefence.this.pauseResumeMenuItem.setText("Resume");
		    AntiTowerDefence.this.contrPane.setPaused(true);
		}
	    }
	});

	JMenuItem showHiscoreMenuItem = new JMenuItem("Show Highscores");
	// The following was writen by Leonard Jakobsson, c13ljn
	showHiscoreMenuItem.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		SwingUtilities.invokeLater(new Runnable() {
		    @Override
		    public void run() {
			HighscoreFrame hsf = new HighscoreFrame(true);
			hsf.createShowFrame();
		    }
		});
	    }
	});

	JMenuItem quitMenuItem = new JMenuItem("Quit");
	quitMenuItem.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		System.exit(0);
	    }
	});

	fileMenu.add(newGameMenuItem);
	fileMenu.add(pauseResumeMenuItem);
	fileMenu.add(restartLevelMenuItem);
	fileMenu.add(new JSeparator());
	fileMenu.add(showHiscoreMenuItem);
	fileMenu.add(new JSeparator());
	fileMenu.add(quitMenuItem);

	JMenu helpMenu = new JMenu("Help");

	JMenuItem aboutMenuItem = new JMenuItem("About");
	aboutMenuItem.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		new AboutDialog(AntiTowerDefence.this);
	    }
	});

	JMenuItem helpMenuItem = new JMenuItem("Help");
	helpMenuItem.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent arg0) {
		new HelpDialog(AntiTowerDefence.this);
	    }
	});

	helpMenu.add(aboutMenuItem);
	helpMenu.add(helpMenuItem);

	menuBar.add(fileMenu);
	menuBar.add(helpMenu);

	setJMenuBar(menuBar);

	JPanel contentPane = (JPanel) this.getContentPane();
	contentPane.setLayout(new BorderLayout());
	contentPane.add(new GameCanvas(game), BorderLayout.CENTER);

	contrPane = new ControlPanel(game);
	contrPane.setPaused(true);
	contentPane.add(contrPane, BorderLayout.SOUTH);

	Thread updateThread = new Thread(new Runnable() {

	    private long time = System.currentTimeMillis();
	    private long delta = 33;
	    
	    @Override
	    public void run() {
		while (running) {
		    try {
			time = System.currentTimeMillis();
			if (!AntiTowerDefence.this.game.isPaused()) {
			    AntiTowerDefence.this.game.getWorld().update();
			    AntiTowerDefence.this.contrPane.updateTroopCombobox();
			}
			int temp;
			temp = (int)(time + delta - System.currentTimeMillis());
			if(temp > 0){
			    Thread.sleep(temp);
			}
		    } catch (InterruptedException e) {
			e.printStackTrace();
		    }
		}
	    }
	});

	updateThread.start();
	
	timer = new Timer(1, new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		AntiTowerDefence.this.game.getCanvas().repaint();
	    }
	});

	pack();

    }

    /**
     * The main method calls the constructor of this class with the
     * run arguments and sets the window to be visible.
     * 
     * @param args
     *            The run arguments.
     */
    public static void main(String[] args) {

	AntiTowerDefence antiTowerDefence = new AntiTowerDefence(args);
	antiTowerDefence.setVisible(true);
    }
}
