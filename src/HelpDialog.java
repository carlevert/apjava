/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the help dialog.
 * 
 * HelpDialog.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 9/12-14
 */

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

/**
 * This class is for the help window.
 * 
 * @author Leonard Jakobsson c13ljn
 * @since 9/12-14
 */
public class HelpDialog extends JDialog {

    /**
     * Generated and suggested by eclipse.
     */
    private static final long serialVersionUID = 1397592899954241616L;

    /**
     * The constructor sets up the help dialog box with contents from
     * help.txt.
     * 
     * @param frame
     *            The window to be positioned from.
     */
    public HelpDialog(JFrame frame) {
	super(frame);

	JPanel panel = (JPanel) getContentPane();
	panel.setLayout(new BorderLayout());

	JLabel label = new JLabel("How To Play AntiTowerDefence");
	label.setBorder(new EmptyBorder(10, 10, 10, 10));
	panel.add(label, BorderLayout.NORTH);
	JTextArea textarea = new JTextArea();
	BufferedReader reader;
	try {
	    reader =
		    new BufferedReader(new InputStreamReader(
		        getClass().getClassLoader().getResourceAsStream(
		            "Help.txt")));
	    textarea.read(reader, "Help.txt");
	} catch (FileNotFoundException e1) {
	    System.err.print("Help.txt missing\n");
	} catch (IOException e1) {
	    System.err.print("Can not read from Help.txt\n");
	}

	textarea.setEditable(false);
	textarea.setBorder(new EmptyBorder(10, 10, 10, 10));
	panel.add(textarea, BorderLayout.CENTER);

	JButton button = new JButton("OK");
	button.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		dispose();
	    }
	});

	panel.add(button, BorderLayout.SOUTH);
	pack();
	setVisible(true);
    }

}
