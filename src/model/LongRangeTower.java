/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the tower with longer range.
 * 
 * LongRangeTower.java
 * 
 * Written by: Linus Bleckert
 * 
 * Created: 26/11-14
 */

package model;

/**
 * This class is for the tower with longer range and lower damage.
 * 
 * @author Linus Bleckert c13lbt
 * @since 26/11-14
 *
 */
public class LongRangeTower extends Tower {

    /**
     * This constructor sets the damage and range of the tower.
     */
    public LongRangeTower() {
	damage = 1;
	range = 4;
    }

}
