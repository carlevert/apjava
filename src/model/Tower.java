/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Abstract class for all towers.
 * 
 * Tower.java
 * 
 * Written by: Linus Bleckert
 * 
 * Created: 26/11-14
 */

package model;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.Timer;

/**
 * The abstract class that represents towers.
 * 
 * @author Linus Bleckert c13lbt
 * @since 26/11-14
 *
 */
public abstract class Tower implements Observer {

    /**
     * The current target of the tower.
     */
    protected Troop currentTarget;
    /**
     * The list of possible targets the tower has.
     */
    private CopyOnWriteArrayList<Troop> targets;
    /**
     * A list of the tiles that are within the towers attack range.
     */
    protected CopyOnWriteArrayList<PathTile> rangeList;
    /**
     * The range of the tower.
     */
    protected int range;
    /**
     * The damage of the tower.
     */
    protected int damage;
    /**
     * The tile the tower is placed on.
     */
    protected Tile tile;
    /**
     * Timer used to create intervalls between attacks from the tower.
     */
    private Timer timer;
    /**
     * Delay used to decide how long tower has to wait to be able to
     * attack.
     */
    private int delay;
    /**
     * If the tower is allowed to attack or not.
     */
    private boolean isFiring = false;

    /**
     * The constructor for towers, starts the timer thats used to
     * determine if the tower can attack or not.
     */
    public Tower() {
	targets = new CopyOnWriteArrayList<Troop>();

	timer = new Timer(100, new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		delay = 100 + (int) Math.random() * 1000000;
		if (!isFiring)
		    Tower.this.timer.setDelay(delay * 2);
		else
		    Tower.this.timer.setDelay(delay);
		isFiring = !isFiring;

	    }
	});
	timer.start();

    }

    /**
     * Sets the list of tiles that the tower has within its range.
     * 
     * @param rangeList
     *            An list of PathTiles
     */
    public void setRangeList(CopyOnWriteArrayList<PathTile> rangeList) {
	this.rangeList = rangeList;
    }

    /**
     * Used when the troop walks out of range of the tower, removes it
     * from list of possible targets.
     * 
     * @param troop
     *            A troop
     */
    public void unaimIfCurrentTarget(Troop troop) {
	if (currentTarget == troop)
	    currentTarget = null;

	targets.remove(troop);
    }

    /**
     * Updates the list of targets the tower has and changes the
     * current target if it no longer is in the list.
     */
    private void updateTargets() {

	ArrayList<Troop> rem = new ArrayList<Troop>();
	for (Troop t : targets) {

	    if (!rangeList.contains(t.getCurrentTile())) {
		rem.add(t);
	    }
	}
	for (Troop t : rem) {
	    targets.remove(t);
	    if (currentTarget == t) {
		int temp = targets.size();
		Random rand = new Random();
		if (targets.size() != 0)
		    currentTarget = targets.get(rand.nextInt(temp));
		else {
		    currentTarget = null;
		    break;
		}

	    }
	}
    }

    /**
     * Attacks the troop that the tower is targeting and if it died
     * the current target gets set to null.
     */
    public void updateGame() {
	updateTargets();
	if (isFiring && currentTarget != null) {
	    boolean alive = currentTarget.getDamage(damage);
	    if (!alive) {
		targets.remove(currentTarget);
		currentTarget = null;
	    }
	}
    }

    /**
     * The method that gets called from the observer, the troop, adds
     * the troop to the list of targets if it wasn�t already a
     * possible target.
     */
    @Override
    public void update(Observable pathTile, Object troop) {

	if (!targets.contains(troop))
	    targets.add((Troop) troop);

	if (targets.size() > 0) {
	    currentTarget = targets.get(0);
	}

    }

    /**
     * Used for testing
     * 
     * @return Troop currentTarget
     */
    public Troop getCurrentTarget() {
	return currentTarget;
    }

    /**
     * Returns the tile that the tower is standing on,
     * 
     * @return The tile the tower is standing on.
     */
    public Tile getTile() {
	return tile;
    }

    /**
     * Sets the tile the tower is standing on
     * 
     * @param tile
     *            The tile the tower stands on
     */
    public void setTile(Tile tile) {
	this.tile = tile;
    }

    /**
     * Returns the range of the tower
     * 
     * @return the range of the tower
     */
    public int getRange() {
	return range;
    }

    /**
     * Paints the tower and its laser if the tower is currently
     * attacking.
     * 
     * @param g2d
     *            A 2D Graphics-object
     */
    public void paint(Graphics2D g2d) {

	Image img1 =
	        Toolkit.getDefaultToolkit().getImage(
	            getClass().getClassLoader().getResource("res/tower.png"));
	Image img2 =
	        Toolkit.getDefaultToolkit().getImage(
	            getClass().getClassLoader()
	                      .getResource("res/tower_hot.png"));

	g2d.setStroke(new BasicStroke(2.0f));
	g2d.setColor(java.awt.Color.RED);

	if (isFiring && currentTarget != null) {
	    g2d.drawImage(img2, tile.getPos().getX()
		                * antitd.GameCanvas.TILESIZE,
		tile.getPos().getY() * antitd.GameCanvas.TILESIZE, null);
	    g2d.drawLine(tile.getPos().getX() * antitd.GameCanvas.TILESIZE
		         + antitd.GameCanvas.TILESIZE / 2,
		tile.getPos().getY() * antitd.GameCanvas.TILESIZE
		        + antitd.GameCanvas.TILESIZE / 2,
		currentTarget.getxPixPos() + antitd.GameCanvas.TILESIZE / 2,
		currentTarget.getyPixPos() + antitd.GameCanvas.TILESIZE / 2);
	} else {
	    g2d.drawImage(img1, tile.getPos().getX()
		                * antitd.GameCanvas.TILESIZE,
		tile.getPos().getY() * antitd.GameCanvas.TILESIZE, null);
	}

    }
}