/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the end of the teleport.
 * 
 * EndTeleportTile.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 5/12-14
 */

package model;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * This class is for the end of a teleport tile pair.
 * 
 * @author Leonard Jakobsson c13ljn
 * @since 5/12-14
 */
public class EndTeleportTile extends PathTile {

    /** The color of the tile. */
    private Color color;

    /**
     * This constructor sets the color to orange.
     */
    public EndTeleportTile() {
	super();
	color = new Color(255, 165, 0);
    }

    /**
     * This method paints an orange rectangle the tiles position.
     */
    @Override
    public void paint(Graphics2D g2d) {
	g2d.setColor(color);
	g2d.fillRect(position.getX() * antitd.GameCanvas.TILESIZE,
	    position.getY() * antitd.GameCanvas.TILESIZE,
	    antitd.GameCanvas.TILESIZE, antitd.GameCanvas.TILESIZE);

    }

}
