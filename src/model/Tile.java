/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Abstract class for all tiles.
 * 
 * Tile.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 1/12-14
 */

package model;

import java.awt.Graphics2D;
import java.util.Observable;

/**
 * The abstract class that represents a tile
 * 
 * @author Leonard Jakobsson c13ljn
 * @since 1/12-14
 * 
 */
public abstract class Tile extends Observable {

    /**
     * The position of this tile
     */
    protected Position position;

    /**
     * Abstract constructor
     */
    public Tile() {
    }

    /**
     * Returns the position of this tile
     * 
     * @return position
     */
    public Position getPos() {
	return position;
    }

    /**
     * Creates a position object with the given x- and y-coordinates,
     * sets this position object to this tile objects instance
     * variable.
     * 
     * @param x
     *            x-coordinate
     * @param y
     *            y-coordinate
     */
    public void setPosition(int x, int y) {
	position = new Position(x, y);
    }

    /**
     * Abstract method for painting tiles.
     * 
     * @param g2d
     *            Graphics to paint with/on
     */
    public abstract void paint(Graphics2D g2d);
}
