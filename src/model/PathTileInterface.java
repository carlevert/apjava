/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * interface with method landOn.
 * 
 * PathTileInterface.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 2/12-14
 */

package model;

/**
 * The interface to be implemented by path tiles.
 * 
 * @author Leonard Jakobsson c13ljn
 * @since 2/12-14
 *
 */
public interface PathTileInterface {

    /**
     * The tower that listened to this tile gets notified of what
     * troop landed on the tile.
     * 
     * @param t
     *            The troop that landed on the tile
     */
    public void landOn(Troop t);

}
