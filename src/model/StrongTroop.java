/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the stronger kind of troop.
 * 
 * StrongTroop.java
 * 
 * Written by: Linus Bleckert
 * 
 * Created: 26/11-14
 */

package model;

import java.awt.Color;

/**
 * This class is for the strong kind of troop.
 * 
 * @author Linus Bleckert c13lbt
 * @since 26/11-14
 */
public class StrongTroop extends Troop {

    /**
     * The constructor that sets the specific attributes for this
     * troop.
     * 
     * @param tile
     *            The start tile
     */
    public StrongTroop(PathTile tile) {
	super(tile);
	credits = 100;
	speed = 10;
	hp = 65;
	color = new Color(255, 245, 238);
	super.setPixels(tile);
    }

}