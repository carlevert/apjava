/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the tower with shorter range and more damage.
 * 
 * ShortRangeTower.java
 * 
 * Written by: Linus Bleckert
 * 
 * Created: 26/11-14
 */

package model;

/**
 * This class is for the tower with shorter range and more damage.
 * This class is currently not implemented, but it shows that the game
 * is easily improved.
 * 
 * @author Linus Bleckert c13lbt
 * @since 26/11-14
 *
 */
public class ShortRangeTower extends Tower {

    /**
     * This constructor sets the damage and range for the tower.
     */
    public ShortRangeTower() {
	damage = 3;
	range = 2;
    }

}
