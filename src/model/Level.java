/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the Levels.
 * 
 * Level.java
 * 
 * Written by: Leonard Jakobsson and Carl-Evert Kangas
 * 
 * Created: 25/11-14
 */

package model;

import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 * This class is for the level and its information.
 * 
 * @author Carl-Evert Kangas dv14cks
 * @author Leonard Jakobsson c13ljn
 * @since 25/11-14
 *
 */
public class Level {

    /** The grid of tiles. */
    private Tile[][] tiles;

    /** The width of the grid. */
    private int width;

    /** The Height of the grid. */
    private int height;

    /** The number of towers to be placed on this level. */
    private int noTowers;

    /** The score to reach. */
    private int goalScore;

    /** The credits to start with. */
    private int startCredit;

    /** The x coordinate for start. */
    private int startTileX;

    /** The y coordinate for start. */
    private int startTileY;

    /** The x coordinate for goal. */
    private int goalTileX;

    /** The y coordinate for goal. */
    private int goalTileY;

    /**
     * This method returns the x coordinate to start at.
     * 
     * @return startTileX
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public int getStartTileX() {
	return startTileX;
    }

    /**
     * This method returns the y coordinate to start at.
     * 
     * @return startTileY
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public int getStartTileY() {
	return startTileY;
    }

    /**
     * This method sets the number of towers to be placed.
     * 
     * @param noTowers
     *            The amount of towers.
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public void setNoTowers(int noTowers) {
	this.noTowers = noTowers;
    }

    /**
     * This method returns the number of towers to be placed.
     * 
     * @return noTowers
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public int getNoTowers() {
	return this.noTowers;
    }

    /**
     * This method returns the score needed to complete the level.
     * 
     * @return goalScore
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public int getGoalScore() {
	return goalScore;
    }

    /**
     * This method returns the amount of credits to start with.
     * 
     * @return startCredit
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public int getStartCredit() {
	return startCredit;
    }

    /**
     * This method checks if the parameter tile is on the same
     * coordinates as the goal.
     * 
     * @param pathTile
     *            The tile to compare with.
     * @return true/false
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public boolean isGoalTile(PathTile pathTile) {
	return pathTile.getPos().getX() == goalTileX
	       && pathTile.getPos().getY() == goalTileY;

    }

    /**
     * This method sets the width of the level.
     * 
     * @param width
     *            The width of the level.
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public void setWidth(int width) {
	this.width = width;
    }

    /**
     * This method sets the height of the level.
     * 
     * @param height
     *            The height of the level.
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public void setHeight(int height) {
	this.height = height;
    }

    /**
     * This method returns the width of the level.
     * 
     * @return width
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public int getWidth() {
	return this.width;
    }

    /**
     * This method returns the height of the level.
     * 
     * @return height
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public int getHeight() {
	return this.height;
    }

    /**
     * This method sets the start credit on the level to the value of
     * the parameter.
     * 
     * @param startCredit
     *            The amount of start credits
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public void setStartCredit(int startCredit) {
	this.startCredit = startCredit;
    }

    /**
     * This method sets the position of the start tile.
     * 
     * @param x
     *            The x coordinate
     * @param y
     *            The y coordinate
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public void setStartTile(int x, int y) {
	this.startTileX = x;
	this.startTileY = y;
    }

    /**
     * This method sets the position of the goal tile.
     * 
     * @param x
     *            The x coordinate
     * @param y
     *            The y coordinate
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public void setGoalTile(int x, int y) {
	this.goalTileX = x;
	this.goalTileY = y;
    }

    /**
     * This method sets the score to reach to complete a level.
     * 
     * @param goalScore
     *            The amount of points to reach
     * @author Carl-Evert Kangas dv14cks
     */
    public void setGoalScore(int goalScore) {
	this.goalScore = goalScore;
    }

    /**
     * This method sets the two dimensional array of tiles and their
     * next tile. Only called when level is loaded.
     * 
     * @param t
     *            Two dimensional tile array.
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public void setTiles(Tile[][] t) {
	this.tiles = t;

	for (int i = 0; i < this.height; i++)
	    for (int j = 0; j < this.width; j++) {
		if (tiles[i][j] instanceof PathTile) {
		    PathTile temp = (PathTile) tiles[i][j];
		    int nextX = temp.getNextTileX();
		    int nextY = temp.getNextTileY();
		    if (nextY != -1 && nextX != -1)
			temp.setNextTile((PathTile) tiles[nextY][nextX]);
		}
	    }
    }

    /**
     * This method returns the two dimensional tile array.
     * 
     * @return tiles
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public Tile[][] getTiles() {
	return this.tiles;
    }

    /**
     * This method replaces a tile in the two dimensional tile array
     * with the tile from the parameter list.
     * 
     * @param tile
     *            The tile to place into the array.
     * 
     * @author Leonard Jakobsson c13ljn
     */
    public synchronized void setTile(Tile tile) {
	if (tile instanceof StartTeleportTile) {
	    for (Tile[] ta : tiles) {
		for (Tile t : ta) {
		    if (t instanceof SwitchTile) {
			((SwitchTile) t).checkAndSetExit(tile);
		    }
		    if (t instanceof PathTile) {
			if (((PathTile) t).getNextTile() == tiles[tile.getPos()
			                                              .getY()][tile.getPos()
			                                                           .getX()]) {
			    PathTile ti =
				    ((PathTile) t).getNextTile().getNextTile();
			    ((PathTile) t).setNextTile((PathTile) tile);
			    ((StartTeleportTile) tile).setNextTile(ti);
			}
		    }
		}
	    }
	}

	if (tile instanceof EndTeleportTile) {
	    ((EndTeleportTile) tile).setNextTile((((PathTile) tiles[tile.getPos()
		                                                        .getY()][tile.getPos()
		                                                                     .getX()]).getNextTile()));
	}

	tiles[tile.getPos().getY()][tile.getPos().getX()] = tile;
    }

    /**
     * THis method returns a list of all tiles where a tower can be
     * placed.
     * 
     * @return possibleTowerTiles
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public ArrayList<PossibleTowerTile> getPossibleTowerTiles() {
	ArrayList<PossibleTowerTile> possibleTowerTiles =
	        new ArrayList<PossibleTowerTile>();
	for (int i = 0; i < this.height; i++)
	    for (int j = 0; j < this.width; j++)
		if (tiles[i][j] instanceof PossibleTowerTile)
		    possibleTowerTiles.add((PossibleTowerTile) tiles[i][j]);
	return possibleTowerTiles;
    }

    /**
     * This method paints the tiles.
     * 
     * @param g2d
     *            The graphics to paint from.
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public synchronized void paint(Graphics2D g2d) {
	for (int i = 0; i < this.width; i++)
	    for (int j = 0; j < this.height; j++) {
		tiles[j][i].paint(g2d);
	    }
    }
}
