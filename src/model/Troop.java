/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Abstract class for all troops.
 * 
 * Troop.java
 * 
 * Written by: Linus Bleckert
 * 
 * Created: 24/11-14
 */

package model;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

/**
 * Abstract class used for troops.
 * 
 * @author Linus Bleckert c13lbt
 * @since 24/11-14
 */
public abstract class Troop {

    /**
     * The tile that the troop is currently landed on.
     */
    private PathTile currentTile;
    /**
     * The next tile of the tile that the troop is currently on.
     */
    private PathTile nextTile;
    /**
     * The speed of which the troop has.
     */
    protected int speed;
    /**
     * How long the troop has been alive, relative to timesteps.
     */
    protected int lifetime;
    /**
     * How many hitpoints the troop has.
     */
    protected int hp;
    /**
     * How many credits needed to buy the troop.
     */
    protected int credits;
    /**
     * The color of the troop.
     */
    protected Color color;
    /**
     * The change on the X-axis divided by the speed of the troop.
     */
    private double xChange;
    /**
     * The change on Y-axis divided by the speed of the troop.
     */
    private double yChange;
    /**
     * The current PixelPosition of the troop on X-axis.
     */
    private int xPixPos;
    /**
     * The current PixelPosition of the troop on Y-axis.
     */
    private int yPixPos;

    /**
     * The constructor for this class, sets its lifetime to 0.
     * 
     * @param tile
     *            The tile that the troop started on.
     */
    public Troop(PathTile tile) {
	this.currentTile = tile;
	nextTile = currentTile.nextTile;
	this.lifetime = 0;

    }

    /**
     * Used for testing. Returns the next tile
     * 
     * @return nextTile
     */
    public PathTile getNextTile() {
	return nextTile;
    }

    /**
     * Returns credits
     * 
     * @return credits
     */
    public int getCredits() {
	return credits;
    }

    /**
     * Takes the tile that the troop landed on and sets the attribute
     * xChange to the change on the X-axis from the current tile
     * compared to the next tile, this is either -1 0 or 1, divides
     * this by the speed that the troop has. Does the same for yChange
     * but the with Y-axis.
     * 
     * @param tile
     *            The tile that the troop landed on.
     */
    protected void setPixels(PathTile tile) {
	if (tile.nextTile == null)
	    return;
	xChange = nextTile.getPos().getX() - tile.getPos().getX();
	xChange = xChange / speed;

	yChange = nextTile.getPos().getY() - tile.getPos().getY();
	yChange = yChange / speed;
    }

    /**
     * This method is invoked every time the game updates and when the
     * game has been updated as many times as the troops speed then
     * the troop will move to the next tile. The troops pixelpositions
     * are also updated relative to how close the troop is to be moved
     * to the next tile.
     */
    public void update() {
	lifetime++;

	if ((lifetime % speed) == 0) {
	    if (nextTile != null) {
		nextTile.landOn(this);
		setPixels(currentTile);
	    }
	}

	xPixPos =
	        (int) ((currentTile.getPos().getX() + xChange
	                                              * (lifetime % speed)) * antitd.GameCanvas.TILESIZE);
	yPixPos =
	        (int) ((currentTile.getPos().getY() + yChange
	                                              * (lifetime % speed)) * antitd.GameCanvas.TILESIZE);

    }

    /**
     * Returns the hp of the troop
     * 
     * @return hp
     */
    public int getHp() {
	return hp;
    }

    /**
     * Returns the pixelposition on X-axis of the troop.
     * 
     * @return xPixPos
     */
    public int getxPixPos() {
	return xPixPos;
    }

    /**
     * Returns the pixelposition on Y-axis of the troop.
     * 
     * @return yPixPos
     */
    public int getyPixPos() {
	return yPixPos;
    }

    /**
     * Returns the tile that the troop is standing on.
     * 
     * @return currentTile
     */
    public Tile getCurrentTile() {
	return currentTile;
    }

    /**
     * Sets the current tile to its next tile, and then does the same
     * with nexttile.
     */
    public void setCurrentTileToNext() {
	currentTile = nextTile;
	nextTile = currentTile.nextTile;
    }

    /**
     * Reduces the amount of hp the troop has by the amount of its
     * parameter.
     * 
     * @param damage
     *            number of hp to be lost
     * @return boolean returns true if the troop is still alive, false
     *         otherwise.
     */
    public boolean getDamage(int damage) {
	hp -= damage;
	if (hp < 0) {
	    hp = 0;
	    return false;
	} else
	    return true;
    }

    /**
     * Paints the troop and its health.
     * 
     * @param g2d
     *            The graphics to paint from
     */
    public void paint(Graphics2D g2d) {
	g2d.setColor(color);
	g2d.fillOval(xPixPos, yPixPos, antitd.GameCanvas.TILESIZE,
	    antitd.GameCanvas.TILESIZE);
	g2d.setColor(java.awt.Color.BLACK);

	Font font = new Font("Sans", Font.PLAIN, 11);
	g2d.setFont(font);

	Image img1 =
	        Toolkit.getDefaultToolkit().getImage(
	            getClass().getClassLoader().getResource("res/heart.png"));
	g2d.drawImage(img1, xPixPos, yPixPos, null);
	g2d.setColor(java.awt.Color.BLACK);
	g2d.drawString(Integer.toString(this.hp), 12 + xPixPos, 8 + yPixPos);

    }
}