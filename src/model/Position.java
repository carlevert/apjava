/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for positioning.
 * 
 * Position.java
 * 
 * Written by: Leonard Jakobsson and Ludvig Bostr�m
 * 
 * Created: 24/11-14
 */

package model;

/**
 * This class is for the position.
 * 
 * @author Leonard Jakobsson c13ljn
 * @author Ludvig Bostrom c13lbm
 * @since 24/11-14
 *
 */
public class Position {

    /** The y coordinate. */
    private int x;

    /** The y coordinate. */
    private int y;

    /**
     * This constructor sets the instance variables x and y to the
     * parameters.
     * 
     * @param x
     *            The x coordinate.
     * @param y
     *            The y coordinate.
     * 
     * @author Leonard Jakobsson c13ljn
     */
    public Position(int x, int y) {
	this.x = x;
	this.y = y;
    }

    /**
     * This method returns the x coordinate.
     * 
     * @return x
     * 
     * @author Leonard Jakobsson c13ljn
     */
    public int getX() {
	return x;
    }

    /**
     * This method returns the y coordinate.
     * 
     * @return y
     * 
     * @author Leonard Jakobsson c13ljn
     */
    public int getY() {
	return y;
    }

    /**
     * This method returns the distance from this position to the
     * parameter position.
     * 
     * @param p
     *            Position to measure to.
     * @return Distance to p.
     * @author Ludvig Bostrom c13lbm
     */
    public double getDistance(Position p) {
	double x2 = Math.pow((x - p.getX()), 2);
	double y2 = Math.pow((y - p.getY()), 2);
	return Math.sqrt(x2 + y2);
    }
}
