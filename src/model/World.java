/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for keeping track on the games components.
 * 
 * World.java
 * 
 * Written by: Ludvig Bostrom and Carl-Evert Kangas
 * 
 * Created: 26/11-14
 */

package model;

import java.awt.Graphics2D;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JOptionPane;

import antitd.Game;
import antitd.HighscoreFrame;

/**
 * This is the class for the world which contains information about
 * the troops and towers.
 * 
 * @author Ludvig Bostrom c13lbm
 * @author Carl-Evert Kangas dv14cks
 * @since 26/11-14
 *
 */
public class World {

    /** The users game data. */
    private Game game;

    /** The level that is played. */
    private Level level;

    /** The list of tiles that towers can be placed on. */
    private ArrayList<PossibleTowerTile> possibleTowerTiles;

    /** The list of living troops. */
    private CopyOnWriteArrayList<Troop> troops;

    /** The list of placed towers. */
    private ArrayList<Tower> towers;

    /** The amount of teleport troops that has been sent. */
    private int nrTeleTroops;

    /**
     * The living teleport troops, troop is key and the value is the
     * number it has.
     */
    private ConcurrentHashMap<TeleportTroop, Integer> teleTroops;

    /** The troops that has placed the start of the teleport. */
    private ConcurrentHashMap<String, StartTeleportTile> troopTiles;

    /** The players score on the level. */
    private int levelScore;

    /**
     * The price of the cheapest troop. This is a last minute
     * solution, recommended to be changed.
     */
    private int cheapestTroop = 100;

    /**
     * This constructor sets the default values and places the towers
     * to the field.
     * 
     * @param level
     *            The level that is played.
     * @param game
     *            The user data of the game.
     * @author Ludvig Bostrom c13lbm
     */
    public World(Level level, Game game) {
	this.level = level;
	this.game = game;
	this.levelScore = 0;
	possibleTowerTiles = level.getPossibleTowerTiles();
	troopTiles = new ConcurrentHashMap<String, StartTeleportTile>();
	towers = new ArrayList<Tower>();

	placeTowers(level.getNoTowers());

	troops = new CopyOnWriteArrayList<Troop>();
	teleTroops = new ConcurrentHashMap<TeleportTroop, Integer>();
	nrTeleTroops = 0;
    }

    /**
     * This method adds a specific troop to the game and removes
     * credits from the user.
     * 
     * @param troopClassName
     *            The kind of troop to be placed.
     * @author Ludvig Bostrom c13lbm
     * @author Carl-Evert Kangas dv14cks
     */
    public void addTroop(String troopClassName) {
	try {
	    Constructor<?> constructor =
		    Class.forName(troopClassName)
		         .getConstructor(PathTile.class);
	    Troop troop =
		    (Troop) constructor.newInstance(level.getTiles()[level.getStartTileY()][level.getStartTileX()]);

	    if (game.getCredit() - troop.getCredits() >= 0) {
		troops.add(troop);
		game.setCredit(game.getCredit() - troop.getCredits());
	    } else {
		return;
	    }

	    if (troopClassName.endsWith("TeleportTroop")) {
		nrTeleTroops++;
		teleTroops.put((TeleportTroop) troop, nrTeleTroops);
	    }

	} catch (ClassNotFoundException e) {
	    // TODO Auto-generated catch block
	    System.err.print("Error occured: " + e + "\n");
	} catch (InstantiationException e) {
	    // TODO Auto-generated catch block
	    System.err.print("Error occured: " + e + "\n");
	} catch (IllegalAccessException e) {
	    // TODO Auto-generated catch block
	    System.err.print("Error occured: " + e + "\n");
	} catch (NoSuchMethodException e) {
	    // TODO Auto-generated catch block
	    System.err.print("Error occured: " + e + "\n");
	} catch (SecurityException e) {
	    // TODO Auto-generated catch block
	    System.err.print("Error occured: " + e + "\n");
	} catch (IllegalArgumentException e) {
	    // TODO Auto-generated catch block
	    System.err.print("Error occured: " + e + "\n");
	} catch (InvocationTargetException e) {
	    // TODO Auto-generated catch block
	    System.err.print("Error occured: " + e + "\n");
	}
    }

    /**
     * This method returns the list of tiles that is within the range
     * of the given tower. The tower is then added as observer of the
     * tile.
     * 
     * @param t
     *            The tile the tower is placed on.
     * @param range
     *            The range of the tower.
     * @param tow
     *            The tower to calculate around.
     * @return tiles.
     * @author Ludvig Bostrom c13lbm
     */
    public CopyOnWriteArrayList<PathTile> generateRangeArray(Tile t, int range, Tower tow) {
	CopyOnWriteArrayList<PathTile> tiles = new CopyOnWriteArrayList<PathTile>();
	Tile[][] tileGrid = level.getTiles();

	for (int i = 0; i < tileGrid.length; i++) {
	    for (int j = 0; j < tileGrid[0].length; j++) {
		if (tileGrid[i][j] instanceof PathTile) {

		    if (t.getPos().getDistance(tileGrid[i][j].getPos()) <= range) {
			tileGrid[i][j].addObserver(tow);
			tiles.add((PathTile) tileGrid[i][j]);
		    }
		}
	    }
	}
	return tiles;
    }

    /**
     * This method returns the level that is played.
     * 
     * @return level
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public Level getLevel() {
	return level;
    }

    /**
     * This method return the score the player got on the level.
     * 
     * @return levelScore
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public int getLevelScore() {
	return levelScore;
    }

    /**
     * This method returns a list of the names of the living teleport
     * troops.
     * 
     * @return teleList
     * @author Ludvig Bostrom c13lbm
     */
    public ArrayList<String> getTeleTroops() {
	ArrayList<String> teleList = new ArrayList<String>();
	for (Integer i : teleTroops.values()) {
	    teleList.add("TeleportTroop" + i);
	}
	return teleList;
    }

    /**
     * This method returns all the placed tower. Primarily used for
     * testing
     * 
     * @return towers
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public ArrayList<Tower> getTowers() {
	return towers;
    }

    /**
     * This method returns all the alive troops. Primarily used for
     * testing
     * 
     * @return troops
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public CopyOnWriteArrayList<Troop> getTroops() {
	return troops;
    }

    /**
     * This method calls the paint-methods within level and for each
     * troop and tower.
     * 
     * @param g2d
     *            The graphics to paint from.
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public void paint(Graphics2D g2d) {
	level.paint(g2d);

	for (Troop troop : troops)
	    troop.paint(g2d);
	for (Tower tower : towers)
	    tower.paint(g2d);
    }

    /**
     * This method places the teleport tiles of given troop.
     * 
     * @param troop
     *            The name of the troop to place the tile.
     * @param end
     *            The end of the teleport. 1 = end. 0 = start.
     * @author Ludvig Bostrom c13lbm
     */
    public void placeTeleport(String troop, int end) {
	char c = troop.charAt(troop.length() - 1);
	int value = Character.getNumericValue(c);
	if (end == 1) {
	    StartTeleportTile stt = troopTiles.get(troop);
	    EndTeleportTile ett = new EndTeleportTile();
	    for (TeleportTroop tele : teleTroops.keySet()) {
		if (teleTroops.get(tele) == value) {
		    ett.setPosition(tele.getCurrentTile().getPos().getX(),
			tele.getCurrentTile().getPos().getY());
		    level.setTile(ett);
		    stt.activate(ett);
		    troopTiles.remove(troop);
		    break;
		}
	    }

	} else if (end == 0) {

	    StartTeleportTile stt = new StartTeleportTile();
	    for (TeleportTroop tele : teleTroops.keySet()) {

		if (teleTroops.get(tele) == value) {
		    stt.setPosition(tele.getCurrentTile().getPos().getX(),
			tele.getCurrentTile().getPos().getY());
		    troopTiles.put(troop, stt);

		    level.setTile(stt);
		    break;
		}
	    }

	}
    }

    /**
     * This method places the amount of towers from the parameter to
     * the field. Towers can be placed on the same position.
     * 
     * @param nrTowers
     *            The amount of towers to be placed
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public void placeTowers(int nrTowers) {
	for (int i = 0; i < nrTowers; i++) {
	    Random rand = new Random();
	    int index = rand.nextInt(possibleTowerTiles.size());
	    PossibleTowerTile tTile = possibleTowerTiles.get(index);
	    Tower tow = new LongRangeTower();
	    tow.setTile(tTile);
	    tow.setRangeList(generateRangeArray(tTile, tow.getRange(), tow));
	    towers.add(tow);
	}
    }

    /**
     * This method updates the game. If a troop dies or comes to goal
     * it's removed and if goal score is reached it will ask if the
     * player wants to go to the next level.
     * 
     * @author Carl-Evert Kangas dv14cks
     * 
     */
    public void update() {

	if (levelScore >= game.getGoalScore()) {

	    if (game.hasNextLevel()) {

		int playNextLevel =
		        JOptionPane.showConfirmDialog(null,
		            "Do you want to continue to the next level?",
		            "Level complete", JOptionPane.YES_NO_OPTION);

		if (playNextLevel == JOptionPane.YES_OPTION) {
		    game.addScore(levelScore);
		    game.nextLevel();
		    teleTroops = new ConcurrentHashMap<TeleportTroop, Integer>();
		} else {
		    game.restartLevel();
		    teleTroops = new ConcurrentHashMap<TeleportTroop, Integer>();
		}

	    } else {
		JOptionPane.showMessageDialog(null, "You won.");
		game.addScore(levelScore);
		HighscoreFrame hsf = new HighscoreFrame();
		hsf.createEnterFrame(game.getScore());
		game.restartGame();
	    }

	} else if (game.getCredit() < cheapestTroop && troops.size() == 0) {
	    if (levelScore <= game.getGoalScore()) {
		game.addScore(levelScore);
		JOptionPane.showMessageDialog(null, "You lose!");
		HighscoreFrame hsf = new HighscoreFrame();
		hsf.createEnterFrame(game.getScore());
		game.restartGame();
	    }
	}

	for (int i = 0; i < troops.size(); i++) {

	    if (level.isGoalTile((PathTile) troops.get(i).getCurrentTile())) {
		levelScore += troops.get(i).getHp();
		for (Tower tower : towers)
		    tower.unaimIfCurrentTarget(troops.get(i));
		teleTroops.remove(troops.get(i));
		troops.remove(i);

	    } else if (troops.get(i).getHp() == 0) {
		teleTroops.remove(troops.get(i));
		troops.remove(i);
	    } else {
		troops.get(i).update();
	    }
	}

	for (Tower tower : towers)
	    tower.updateGame();

    }
}