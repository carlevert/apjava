/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the tiles at crossings.
 * 
 * SwitchTile.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 3/12-14
 */

package model;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 * A switch with multiple exits, only one exit can be active at one
 * point in time.
 * 
 * @author Leonard Jakobsson c13ljn
 * @since 3/12-14
 */
public class SwitchTile extends PathTile {

    /**
     * A list with exits.
     */
    private ArrayList<PathTile> exits;

    /**
     * The color for the active arrow to a exit
     */
    private Color activeArrowColor;

    /**
     * The color for the unactive arrow to a exit
     */
    private Color inactiveArrowColor;

    /**
     * The constructor, sets the active arrows color to green and the
     * inactive arrows color to gray.
     */
    public SwitchTile() {

	activeArrowColor = new Color(144, 238, 144);
	inactiveArrowColor = new Color(150, 150, 150);
	exits = new ArrayList<PathTile>();
    }

    /**
     * Sets the list of exits
     * 
     * @param exits
     *            The list of available exits
     */
    public void setExits(ArrayList<PathTile> exits) {
	this.exits = exits;
    }

    /**
     * Changes which exit is active.
     */
    public void toggle() {
	int i = exits.indexOf(nextTile);
	if ((i == -1) || (i == exits.size() - 1)) {
	    i = 0;
	} else {
	    i++;
	}
	nextTileX = exits.get(i).getPos().getX();
	nextTileY = exits.get(i).getPos().getY();
	setNextTile(exits.get(i));
    }

    /**
     * Calls the paint method in Tile and draws lines from the tile to
     * the exits in an appropriate color (depending on if the exit is
     * the next tile or not)
     */
    public void paint(Graphics2D g2d) {

	super.paint(g2d);

	int startX =
	        getPos().getX() * antitd.GameCanvas.TILESIZE
	                + antitd.GameCanvas.TILESIZE / 2;
	int startY =
	        getPos().getY() * antitd.GameCanvas.TILESIZE
	                + antitd.GameCanvas.TILESIZE / 2;
	for (PathTile t : exits) {
	    g2d.setStroke(new BasicStroke(3.0f));
	    if (t == nextTile) {
		g2d.setColor(activeArrowColor);
	    } else {
		g2d.setColor(inactiveArrowColor);
	    }
	    int endX =
		    t.getPos().getX() * antitd.GameCanvas.TILESIZE
		            + antitd.GameCanvas.TILESIZE / 2;
	    int endY =
		    t.getPos().getY() * antitd.GameCanvas.TILESIZE
		            + antitd.GameCanvas.TILESIZE / 2;
	    if (startY - endY == 0) {
		// across
		endX = (startX + endX) / 2;
	    } else {
		// vertical
		endY = (startY + endY) / 2;
	    }

	    g2d.drawLine(startX, startY, endX, endY);
	}
    }

    /**
     * Used if an exit tile (may) gets changed.
     * 
     * @param tile
     *            the changed tile that may be an exit
     */
    public void checkAndSetExit(Tile tile) {
	for (Tile e : exits) {
	    if ((e.getPos().getX() == tile.getPos().getX())
		&& (e.getPos().getY() == tile.getPos().getY())) {
		exits.remove(e);
		exits.add((PathTile) tile);
		break;
	    }
	}
    }
}
