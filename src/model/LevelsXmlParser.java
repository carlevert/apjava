/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for reading the levels xml file.
 * 
 * LevelsXmlParser.java
 * 
 * Written by: Leonard Jakobsson, Ludvig Bostr�m and Carl-Evert Kangas
 * 
 * Created: 9/12-14
 */

package model;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This class is for parsing the xml file containing the levels.
 * 
 * @author Ludvig Bostrom c13lbm
 * @author Leonard Jakobsson c13ljn
 * @author Carl-Evert Kangas dv14cks
 * @since 2/12-14
 *
 */
public class LevelsXmlParser {

    /** The list of all levels. */
    private ArrayList<Level> levels;

    /**
     * This constructor validates and collects the data for each
     * level.
     * 
     * @param filename
     *            The file to parse
     * @author Leonard Jakobsson c13ljn
     * @author Carl-Evert Kangas dv14cks
     */
    public LevelsXmlParser(String filename) {

	try {
	    SchemaFactory fac =
		    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	    Schema sch =
		    fac.newSchema(getClass().getClassLoader().getResource(
		        "levelSchema.xsd"));
	    javax.xml.validation.Validator val = sch.newValidator();
	    Source source =
		    new StreamSource(getClass().getClassLoader()
		                               .getResourceAsStream(filename));
	    val.validate(source);
	} catch (SAXException | IOException e1) {
	    System.err.print("Error occured: " + e1 + "\n");
	    System.exit(1);
	}

	this.levels = new ArrayList<Level>();

	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	DocumentBuilder builder;
	try {
	    builder = factory.newDocumentBuilder();

	    Document document =
		    builder.parse(getClass().getClassLoader()
		                            .getResourceAsStream(filename));

	    document.getDocumentElement().normalize();
	    document.normalize();

	    NodeList levels = document.getElementsByTagName("level");

	    for (int i = 0; i < levels.getLength(); i++) {

		this.levels.add(i, new Level());

		NodeList level = levels.item(i).getChildNodes();

		for (int j = 0; j < level.getLength(); j++) {
		    if (level.item(j) instanceof Element) {

			Element element = (Element) level.item(j);

			switch (element.getNodeName()) {

			case "dimension":
			    this.levels.get(i)
				       .setWidth(
				           Integer.parseInt(element.getAttribute("width")));
			    this.levels.get(i)
				       .setHeight(
				           Integer.parseInt(element.getAttribute("height")));
			    break;
			case "starttile":
			    this.levels.get(i).setStartTile(
				Integer.parseInt(element.getAttribute("x")),
				Integer.parseInt(element.getAttribute("y")));
			    break;
			case "goaltile":
			    this.levels.get(i).setGoalTile(
				Integer.parseInt(element.getAttribute("x")),
				Integer.parseInt(element.getAttribute("y")));
			    break;
			case "startcredit":
			    this.levels.get(i).setStartCredit(
				Integer.parseInt(element.getTextContent()));
			    break;
			case "goalscore":
			    this.levels.get(i).setGoalScore(
				Integer.parseInt(element.getTextContent()));
			    break;
			case "towers":
			    this.levels.get(i).setNoTowers(
				Integer.parseInt(element.getTextContent()));
			    break;
			case "grid":
			    this.levels.get(i).setTiles(
				loadTiles(element, this.levels.get(i)
				                              .getWidth(),
				    this.levels.get(i).getHeight()));
			    break;
			}
		    }
		}
	    }
	} catch (ParserConfigurationException | SAXException | IOException e) {
	    System.err.print("Error occured: " + e + "\n");
	}
    }

    /**
     * This method returns the list of levels.
     * 
     * @return levels
     * 
     * @author Carl-Evert Kangas dv14cks
     */
    public ArrayList<Level> getLevels() {
	return this.levels;
    }

    /**
     * This method sets up a two dimensional tile array by looping
     * through the grid and collecting the tiles data.
     * 
     * @param e
     *            The element to loop through
     * @param width
     *            The width of the grid
     * @param height
     *            The height of the grid
     * @return tiles
     * 
     * @author Ludvig Bostrom c13lbm
     * @author Carl-Evert Kangas dv14cks
     */
    private Tile[][] loadTiles(Element e, int width, int height) {

	Tile[][] tiles = new Tile[height][width];

	NodeList rowList = e.getChildNodes();
	for (int i = 0; i < rowList.getLength(); i++) {
	    if (rowList.item(i) instanceof Element
		&& rowList.item(i).getNodeName().equals("row")) {
		NodeList colList = rowList.item(i).getChildNodes();
		for (int j = 0; j < colList.getLength(); j++) {
		    if (colList.item(j) instanceof Element) {

			int x;
			int y;
			Element theElement = (Element) colList.item(j);
			x = Integer.parseInt(theElement.getAttribute("x"));
			y = Integer.parseInt(theElement.getAttribute("y"));

			Class<?> tileClass;
			try {
			    tileClass =
				    Class.forName("model."
				                  + colList.item(j)
				                           .getNodeName());
			    tiles[y][x] = (Tile) tileClass.newInstance();
			    tiles[y][x].setPosition(x, y);
			    if (tiles[y][x] instanceof PathTile) {

				NodeList nextPositions =
				        theElement.getChildNodes();
				for (int n = 0; n < nextPositions.getLength(); n++)
				    if (nextPositions.item(n) instanceof Element
					&& nextPositions.item(n).getNodeName()
					                .equals("next")) {

					Element nextPosition =
					        (Element) nextPositions.item(n);
					if (!(tiles[y][x] instanceof SwitchTile)
					    || Boolean.parseBoolean(nextPosition.getAttribute("default"))) {
					    ((PathTile) tiles[y][x]).setNextTileX(Integer.parseInt(nextPosition.getAttribute("x")));
					    ((PathTile) tiles[y][x]).setNextTileY(Integer.parseInt(nextPosition.getAttribute("y")));
					}
				    }
			    }
			} catch (ClassNotFoundException
			        | InstantiationException
			        | IllegalAccessException e1) {

			    System.err.print("Error occured: " + e1 + "\n");
			}
		    }

		}
	    }
	}

	for (int i = 0; i < rowList.getLength(); i++) {
	    if (rowList.item(i) instanceof Element
		&& rowList.item(i).getNodeName().equals("row")) {
		NodeList colList = rowList.item(i).getChildNodes();
		for (int j = 0; j < colList.getLength(); j++) {
		    if (colList.item(j) instanceof Element) {

			int x;
			int y;
			Element theElement = (Element) colList.item(j);
			x = Integer.parseInt(theElement.getAttribute("x"));
			y = Integer.parseInt(theElement.getAttribute("y"));
			if (tiles[y][x] instanceof SwitchTile) {
			    ArrayList<PathTile> availableExits =
				    new ArrayList<PathTile>();
			    NodeList nextPositions = theElement.getChildNodes();
			    for (int n = 0; n < nextPositions.getLength(); n++) {
				if (nextPositions.item(n) instanceof Element
				    && nextPositions.item(n).getNodeName()
				                    .equals("next")) {
				    Element nextPosition =
					    (Element) nextPositions.item(n);
				    int tempX =
					    Integer.parseInt(nextPosition.getAttribute("x"));
				    int tempY =
					    Integer.parseInt(nextPosition.getAttribute("y"));

				    availableExits.add((PathTile) tiles[tempY][tempX]);

				}
			    }
			    ((SwitchTile) tiles[y][x]).setExits(availableExits);
			}
		    }
		}
	    }
	}

	return tiles;
    }

}
