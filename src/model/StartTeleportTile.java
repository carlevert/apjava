/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the start of the teleport..
 * 
 * StartTeleportTile.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 5/12-14
 */

package model;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * This class is for the start of a teleport tile pair.
 * 
 * @author Leonard Jakobsson c13ljn
 * @since 5/12-14
 */
public class StartTeleportTile extends PathTile {

    /** The color of the tile. */
    private Color color;

    /**
     * The constructor sets the color to blue
     */
    public StartTeleportTile() {
	super();
	color = new Color(0, 0, 255);
    }

    /**
     * Activates the teleport tile pair
     * 
     * @param e
     *            The tile to teleport to
     */
    public void activate(EndTeleportTile e) {
	nextTile = e;
    }

    /**
     * This method paints an blue rectangle on a set position.
     */
    @Override
    public void paint(Graphics2D g2d) {
	g2d.setColor(color);
	g2d.fillRect(position.getX() * antitd.GameCanvas.TILESIZE,
	    position.getY() * antitd.GameCanvas.TILESIZE,
	    antitd.GameCanvas.TILESIZE, antitd.GameCanvas.TILESIZE);

    }

}
