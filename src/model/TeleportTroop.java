/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the troops that can place teleporters.
 * 
 * TeleportTroop.java
 * 
 * Written by: Ludvig Bostr�m
 * 
 * Created: 1/12-14
 */

package model;

import java.awt.Color;

/**
 * This class is the troop that can place a teleporter tile pair.
 * 
 * @author Ludvig Bostrom c13lbm
 * @since 1/12-14
 *
 */
public class TeleportTroop extends Troop {

    /**
     * The constructor that sets the specific attributes for this
     * troop.
     * 
     * @param tile
     *            The start tile.
     * @author Ludvig Bostrom c13lbm
     */
    public TeleportTroop(PathTile tile) {
	super(tile);
	credits = 400;
	speed = 7;
	hp = 50;
	color = new Color(170, 0, 170);
	super.setPixels(tile);
    }
}
