/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the tiles with only terrain.
 * 
 * TerrainTile.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 2/12-14
 */

package model;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

/**
 * The tile-class that represent terrain
 * 
 * @author Leonard Jakobsson c13ljn
 * @since 2/12-14
 */
public class TerrainTile extends Tile {

    /**
     * Paints an image that represents the tile terrain.
     */
    public void paint(Graphics2D g2d) {
	Image img1 =
	        Toolkit.getDefaultToolkit().getImage(
	            getClass().getClassLoader().getResource("res/terrain.png"));

	g2d.drawImage(img1, position.getX() * antitd.GameCanvas.TILESIZE,
	    position.getY() * antitd.GameCanvas.TILESIZE, null);

    }
}
