/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for fast kind of troop.
 * 
 * FastTroop.java
 * 
 * Written by: Ludvig Bostr�m
 * 
 * Created: 26/11-14
 */

package model;

import java.awt.Color;

/**
 * This class is for the fast kind of troop.
 * 
 * @author Ludvig Bostrom c13lbm
 * @since 26/11-14
 *
 */
public class FastTroop extends Troop {

    /**
     * This method sets the type specific values to the troop.
     * 
     * @param tile
     *            The start tile.
     * @author Ludvig Bostrom c13lbm
     */
    public FastTroop(PathTile tile) {
	super(tile);
	credits = 100;
	speed = 3;
	hp = 35;
	color = new Color(255, 182, 193);
	super.setPixels(tile);
    }

}
