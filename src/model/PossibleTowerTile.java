/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the tiles for towers to be placed on.
 * 
 * PossibleTowerTile.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 2/12-14
 */

package model;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

/**
 * This class represents a tile where a tower could possibly be placed
 * on.
 * 
 * @author Leonard Jakobsson c13ljn
 * @since 2/12-14
 */
public class PossibleTowerTile extends Tile {

    /**
     * Paints an image that represents the possibletower tile.
     */
    public void paint(Graphics2D g2d) {
	Image img1 =
	        Toolkit.getDefaultToolkit().getImage(
	            getClass().getClassLoader().getResource(
	                "res/possibletowertile.png"));

	g2d.drawImage(img1, position.getX() * antitd.GameCanvas.TILESIZE,
	    position.getY() * antitd.GameCanvas.TILESIZE, null);

    }
}
