/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the tiles of the path.
 * 
 * PathTile.java
 * 
 * Written by: Leonard Jakobsson
 * 
 * Created: 2/12-14
 */

package model;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

/**
 * The class that represents a tile that is part of a path.
 * 
 * @author Leonard Jakobsson c13ljn
 * @since 2/12-14
 */
public class PathTile extends Tile implements PathTileInterface {

    /**
     * A boolean showing if the tile is the start of a path or not.
     */
    protected boolean startTile;

    /**
     * A boolean showing if the tile is the goal of a path or not.
     */
    protected boolean goalTile;

    /**
     * The next tile in the path
     */
    protected PathTile nextTile;

    /**
     * The X-coordinate of the next tile in the path.
     */
    protected int nextTileX;

    /**
     * The Y-coordinate of the next tile in the path.
     */
    protected int nextTileY;

    /**
     * Set the next tile in the path.
     * 
     * @param nextTile
     *            The next tile in the path
     */
    public void setNextTile(PathTile nextTile) {
	this.nextTile = nextTile;
    }

    /**
     * Returns the next tile in the path
     * 
     * @return nextTile
     */
    public PathTile getNextTile() {
	return this.nextTile;
    }

    /**
     * Set the X-coordinate of the next tile in the path.
     * 
     * @param x
     *            The x coordinate
     */
    public void setNextTileX(int x) {
	this.nextTileX = x;
    }

    /**
     * Set the Y-coordinate of the next tile in the path.
     * 
     * @param y
     *            The y coordinate
     */
    public void setNextTileY(int y) {
	this.nextTileY = y;
    }

    /**
     * Returns the X-coordinate of the next tile in the path.
     * 
     * @return nextTileX
     */
    public int getNextTileX() {
	return this.nextTileX;
    }

    /**
     * Returns the Y-coordinate of the next tile in the path.
     * 
     * @return nextTileY
     */
    public int getNextTileY() {
	return this.nextTileY;
    }

    /**
     * The troops current tile will be set to this tiles next tile and
     * and observers will be notified.
     * 
     * @param t
     *            the troop that landed on this PathTile
     */
    @Override
    public void landOn(Troop t) {
	t.setCurrentTileToNext();
	setChanged();
	notifyObservers(t);
    }

    /**
     * Paints an image representing a tile in a path.
     */
    @Override
    public void paint(Graphics2D g2d) {
	Image img1 =
	        Toolkit.getDefaultToolkit()
	               .getImage(
	                   getClass().getClassLoader().getResource(
	                       "res/pathtile.png"));

	g2d.drawImage(img1, position.getX() * antitd.GameCanvas.TILESIZE,
	    position.getY() * antitd.GameCanvas.TILESIZE, null);

    }

}