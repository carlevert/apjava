/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for redirecting System.err.
 * 
 * ErrStream.java
 * 
 * Written by: Ludvig Bostr�m
 * 
 * Created: 10/12-14
 */

import java.io.IOException;
import java.io.OutputStream;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * This class redirects the System.err stream to a JOptienPane.
 * 
 * @author Ludvig Bostr�m c13lbm
 * @since 10/12-14
 */
public class ErrStream extends OutputStream {

    /** The frame the JOptionPane will be based on. */
    private JFrame frame;

    /** The string with the ERROR message. */
    private String s;

    /**
     * This constructor sets the instance variables to their starting
     * values.
     * 
     * @param f
     *            The JFrame that the JOptionPane should create from.
     */
    public ErrStream(JFrame f) {
	frame = f;
    }

    /**
     * The write method that writes the string to the JOptionPane
     * 
     * @param a
     *            Array of bytes to create the string from.
     * @param off
     *            The offset the string start from.
     * @param len
     *            The length of the string.
     */
    public void write(byte[] a, int off, int len) throws IOException {

	s = new String(a, off, len);

	openJPane();
    }

    /**
     * This is needed to override the write from OutputStream.
     */
    @Override
    public void write(int arg0) throws IOException {

    }

    /**
     * This method opens the JOptionPane with the string and as a
     * ERROR message.
     */
    private void openJPane() {

	JOptionPane.showMessageDialog(frame, s, "Error",
	    JOptionPane.ERROR_MESSAGE);
    }

}