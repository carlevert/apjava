/*
 * Applikationsutveckling i java HT14 5DV135
 * 
 * Used for the about dialog.
 * 
 * AboutDialog.java
 * 
 * Written by: Carl-Evert Kangas
 * 
 * Created: 4/12-14
 */

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

/**
 * Class with the about dialog. Contains information about the game
 * and creators.
 * 
 * @author Carl-Evert Kangas dv14cks
 * @since 4/12-14
 */
@SuppressWarnings("serial")
public class AboutDialog extends JDialog {

    /**
     * The constructor calls the JDialog constructor and then sets up
     * the dialog box.
     * 
     * @param frame
     *            The frame to open from
     */
    public AboutDialog(JFrame frame) {

	super(frame);

	setPreferredSize(new Dimension(400, 300));
	setMinimumSize(new Dimension(400, 300));

	JPanel panel = (JPanel) getContentPane();

	panel.setLayout(new BorderLayout());

	JLabel label = new JLabel("AntiTowerDefence, grupp 8");
	label.setBorder(new EmptyBorder(10, 10, 10, 10));
	panel.add(label, BorderLayout.NORTH);
	JTextArea textarea = new JTextArea();
	textarea.setText("Ludvig Bostr�m c13lbm\n" + "Linus Bleckert c13lbt\n"
	                 + "Leonard Jakobsson c13ljn\n"
	                 + "Carl-Evert Kangas dv14cks\n");
	textarea.setEditable(false);
	textarea.setBorder(new EmptyBorder(10, 10, 10, 10));
	panel.add(textarea, BorderLayout.CENTER);

	JButton button = new JButton("OK");
	button.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		AboutDialog.this.dispose();
	    }
	});

	panel.add(button, BorderLayout.SOUTH);

	setVisible(true);

    }

}
